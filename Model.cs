using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.IO;

namespace Accounting.Model
{
  public class Account
  {
    public const string ASSETS = "Assets";
    public const string LIABILITIES = "Liabilities";
    public const string REVENUE = "Revenue";
    public const string EXPENSES = "Expenses";
    public const string CAPITAL = "Capital";
    public const string SURPLUS = "Surplus";

    private Int32 _AccountCode;
    private String _AccountName;
    private String _Description;
    private String _Type;

    public Int32 Code
    {
      get { return _AccountCode; }
      set { _AccountCode = value; }
    }

    public String AccountName
    {
      get { return _AccountName; }
      set { _AccountName = value; }
    }

    public String Description
    {
      get { return _Description; }
      set { _Description = value; }
    }

    public String Type
    {
      get { return _Type; }
      set { _Type = value; }
    }

    public Account()
    {

    }

    public Account(Int32 code, String name, String description, String type)
    {
      this.Code = code;
      this.AccountName = name;
      this.Description = description;
      this.Type = type;
    }

    public void showAccount()
    {
      Console.WriteLine("{0} \t {1} \t {2} \t {3}", Code, AccountName, Description, Type);
    }

    public string toXmlElement()
    {
      return "<account code=\"" + Code + "\">" + AccountName + "</account>";
    }

    public string nameToXmlElement()
    {
      return "<name>" + AccountName + "</name>";
    }

    public string codeToXmlElement()
    {
      return "<code>" + Code.ToString() + "</code>";
    }

    public string descriptionToXmlElement()
    {
      return "<description>" + Description + "</description>";
    }

    public string typeToXMLElement()
    {
      return "<type>" + Type + "</type>";
    }

    public List<string> getAccountTypes()
    {
      List<string> types = new List<string>();
      types.Add(ASSETS);
      types.Add(REVENUE);
      types.Add(LIABILITIES);
      types.Add(EXPENSES);
      types.Add(CAPITAL);
      types.Add(SURPLUS);

      return types;
    }
  }

  public class Unit
  {
    private DateTime date;

    public DateTime Date
    {
      get { return date; }
      set { date = value; }
    }
    private Int32 amount;

    public Int32 Amount
    {
      get { return amount; }
      set { amount = value; }
    }
    private Int16 id;

    public Int16 Id
    {
      get { return id; }
      set { id = value; }
    }

    public Unit()
    {

    }

    public Unit(DateTime Date, Int32 Amount, Int16 ID)
    {
      this.date = Date;
      this.amount = Amount;
      this.id = ID;
    }
  }

  public class T_Account
  {
    private Account account;

    public Account Account
    {
      get { return account; }
      set { account = value; }
    }
    private List<Unit> debits;

    public List<Unit> Debits
    {
      get { return debits; }
      set { debits = value; }
    }
    private List<Unit> credits;

    public List<Unit> Credits
    {
      get { return credits; }
      set { credits = value; }
    }

    public T_Account()
    {
      this.account = new Account();
      this.debits = new List<Unit>();
      this.credits = new List<Unit>();
    }

    public T_Account(Account account, List<Unit> debits, List<Unit> credits)
    {
      this.account = account;
      this.debits = debits;
      this.credits = credits;
    }

    public void addDebit(Unit unit)
    {
      this.debits.Add(unit);
    }

    public void addCredit(Unit unit)
    {
      this.credits.Add(unit);
    }
  }

  public class JournalEntry
  {
    private Account _DebitedAccount;
    public Account DebitedAccount
    {
      get { return _DebitedAccount; }
      set { _DebitedAccount = value; }
    }

    private Account _CreditedAccount;
    public Account CreditedAccount
    {
      get { return _CreditedAccount; }
      set { _CreditedAccount = value; }
    }

    private String _Description;
    public String Description
    {
      get { return _Description; }
      set { _Description = value; }
    }

    private DateTime _Date;
    public DateTime Date
    {
      get { return _Date; }
      set { _Date = value; }
    }

    private Int32 _Amount;
    public Int32 Amount
    {
      get { return _Amount; }
      set { _Amount = value; }
    }

    public JournalEntry()
    {

    }

    public JournalEntry(Account debitedAccount, Account creditedAccount, String description, DateTime date, Int32 amount)
    {
      this.DebitedAccount = debitedAccount;
      this.CreditedAccount = creditedAccount;
      this.Description = description;
      this.Date = date;
      this.Amount = amount;
    }

    public void showInformation()
    {
      this.DebitedAccount.showAccount();
      Console.WriteLine("Debit: {0}", this.Amount);
      this.CreditedAccount.showAccount();
      Console.WriteLine("Credit: {0}", this.Amount);
      Console.WriteLine(this.Description);
    }

    public void saveInXmlFile(string filename)
    {
      XmlTextReader reader = new XmlTextReader(filename);
      XmlDocument doc = new XmlDocument();
      doc.Load(reader);
      reader.Close();

      XmlNode currNode;
      XmlDocumentFragment docFrag = doc.CreateDocumentFragment();

      string xmlContent = "";
      xmlContent += DebitedAccount.toXmlElement() + "\n" + CreditedAccount.toXmlElement() + "\n";
      xmlContent += "<decsription>" + Description + "</decsription>\n";
      xmlContent += "<date>" + Date.ToString("dd/MM/yy") + "</date>\n";
      xmlContent += "<amount>" + Amount.ToString() + "</amount>\n";
      docFrag.InnerXml = "<entry>\n" + xmlContent + "</entry>";
      // insert the "data" node at the end of the xml file.
      currNode = doc.DocumentElement;
      currNode.InsertAfter(docFrag, currNode.LastChild);
      // save the output to a file
      doc.Save(filename);
    }
  }

  public class GeneralJournal
  {
    private List<JournalEntry> journal;

    public List<JournalEntry> Journal
    {
      get { return journal; }
      set { journal = value; }
    }

    public GeneralJournal()
    {
      journal = new List<JournalEntry>();
    }

    public void dislayAllJournalEntry()
    {
      foreach (JournalEntry entry in journal)
        entry.showInformation();
    }

    public void addJournalEntry(JournalEntry journalEntry)
    {
      journal.Add(journalEntry);
    }
  }

  public class GeneralLedger
  {
    private List<T_Account> ledger;

    public List<T_Account> Ledger
    {
      get { return ledger; }
      set { ledger = value; }
    }

    public GeneralLedger()
    {
      ledger = new List<T_Account>();
    }

    public void addTAccount(T_Account t_account)
    {
      ledger.Add(t_account);
    }
  }

  public class AccountBalance
  {
    private Account _bAccount;
    public Account BAccount
    {
      get { return _bAccount; }
      set { _bAccount = value; }
    }

    private Int32 _debitAmount;

    public Int32 DebitAmount
    {
      get { return _debitAmount; }
      set { _debitAmount = value; }
    }

    private Int32 _creditAmount;

    public Int32 CreditAmount
    {
      get { return _creditAmount; }
      set { _creditAmount = value; }
    }

    public AccountBalance()
    {
      _bAccount = new Account();
      _debitAmount = _creditAmount = 0;
    }

    public AccountBalance(Account bAccount, Int32 debitAmount, Int32 creditAmount)
    {
      this._bAccount = bAccount;
      this._debitAmount = debitAmount;
      this._creditAmount = creditAmount;
    }
  }
}
