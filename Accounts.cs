using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using Accounting.Controller;
using Accounting.Model;
using Accounting.Data;

namespace Accounting.View
{
  public partial class frmAccounts : Form
  {
    string filePath = new ApplicationEnvironment().getApplicationDirectory() + DBConfig.ACCOUNTS_FILE_NAME;
    DataSet ds = new DataSet();
    bool newOperation = false;

    public frmAccounts(frmMainWindow parent)
    {
      InitializeComponent();
      this.MdiParent = parent;
    }

    private void frmAccounts_Load(object sender, EventArgs e)
    {
      loadDataGridView();
      cbxAccountType.DataSource = new Account().getAccountTypes();
    }

    private void dataGridViewAccounts_DoubleClick(object sender, EventArgs e)
    {
      DataGridViewRow dRow = dataGridViewAccounts.CurrentRow;
      tbxAccountCode.Text = dRow.Cells[0].Value.ToString();
      tbxAccountName.Text = dRow.Cells[1].Value.ToString();
      rtbDescription.Text = dRow.Cells[2].Value.ToString();
      cbxAccountType.Text = dRow.Cells[3].Value.ToString();
      btnSave.Enabled = true;
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      if (newOperation)
      {
        newOperation = false;
        addNewAccount();
        btnSave.Enabled = false;
        dataGridViewAccounts.AllowUserToAddRows = false;
        dataGridViewAccounts.AllowUserToDeleteRows = false;
      }
      else
      {
        if (tbxAccountCode.Text != "" && tbxAccountName.Text != "")
        {
          Account account = new Account(Int32.Parse(tbxAccountCode.Text), tbxAccountName.Text,
              rtbDescription.Text, cbxAccountType.Text);
          saveInXmlFile(dataGridViewAccounts.CurrentRow.Index);
          dataGridViewAccounts.Refresh();
          btnSave.Enabled = false;
          dataGridViewAccounts.AllowUserToAddRows = false;
          dataGridViewAccounts.AllowUserToDeleteRows = false;
        }
        else
        {
          MessageBox.Show(this, "Please enter into the textboxes completely!",
              "Accounting System", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
      }
    }

    private void btnDelete_Click(object sender, EventArgs e)
    {
      DialogResult response = MessageBox.Show("Do you really want to delete this account?", "Confirm", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
      if (response == DialogResult.OK)
      {
        btnReset_Click(sender, e);
        dataGridViewAccounts.AllowUserToDeleteRows = true;
        deleteAccount(dataGridViewAccounts.CurrentRow.Index);
        dataGridViewAccounts.AllowUserToDeleteRows = false;
      }
    }

    private void btnReset_Click(object sender, EventArgs e)
    {
      tbxAccountCode.Text = "";
      tbxAccountName.Text = "";
      rtbDescription.Text = "";
      cbxAccountType.Text = "";
      btnSave.Enabled = true;
    }

    private void btnNew_Click(object sender, EventArgs e)
    {
      btnReset_Click(sender, e);
      dataGridViewAccounts.AllowUserToAddRows = true;
      tbxAccountCode.Focus();
      newOperation = true;
    }

    private void addNewAccount()
    {
      //create the reader filestream (fs)
      FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

      //Create the xml document
      XmlDocument CXML = new XmlDocument();

      //Load the xml document
      CXML.Load(fs);

      //Close the fs filestream
      fs.Close();

      // create the new element (node)
      XmlElement newAccount = CXML.CreateElement("account");
      XmlElement code = CXML.CreateElement("code");
      XmlElement name = CXML.CreateElement("name");
      XmlElement desc = CXML.CreateElement("description");
      XmlElement type = CXML.CreateElement("type");
      // Put the value (inner Text) into the node
      code.InnerText = tbxAccountCode.Text;
      name.InnerText = tbxAccountName.Text;
      desc.InnerText = rtbDescription.Text;
      type.InnerText = cbxAccountType.Text;

      newAccount.AppendChild(code);
      newAccount.AppendChild(name);
      newAccount.AppendChild(desc);
      newAccount.AppendChild(type);
      //Insert the new XML Element into the main xml document (CXML)
      CXML.DocumentElement.InsertAfter(newAccount, CXML.DocumentElement.LastChild);

      //Save the XML file
      FileStream WRITER = new FileStream(filePath, FileMode.Truncate, FileAccess.Write, FileShare.ReadWrite);
      CXML.Save(WRITER);

      //Close the writer filestream
      WRITER.Close();

      //Update the contents of the listbox
      loadDataGridView();
    }

    private void deleteAccount(int index)
    {
      //Create FileStream fs
      FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

      //Create new XmlDocument
      XmlDocument xmldoc = new XmlDocument();

      //Load the contents of the filestream into the XmlDocument (xmldoc)
      xmldoc.Load(fs);

      //close the fs filestream
      fs.Close();

      //Remove the xml node
      xmldoc.DocumentElement.RemoveChild(xmldoc.DocumentElement.ChildNodes[index]);

      // Create the filestream for saving
      FileStream WRITER = new FileStream(filePath, FileMode.Truncate, FileAccess.Write, FileShare.ReadWrite);

      // Save the xmldocument
      xmldoc.Save(WRITER);

      //Close the writer filestream
      WRITER.Close();

      //Update the contents of the listbox
      loadDataGridView();
    }

    private void setupColumns()
    {
      DataGridViewTextBoxColumn codeColumn = new DataGridViewTextBoxColumn();
      codeColumn.DataPropertyName = "Code";
      codeColumn.HeaderText = "Account Code";
      codeColumn.ValueType = typeof(Int32);
      codeColumn.Frozen = false;
      dataGridViewAccounts.Columns.Add(codeColumn);

      DataGridViewTextBoxColumn nameColumn = new DataGridViewTextBoxColumn();
      nameColumn.DataPropertyName = "Name";
      nameColumn.Width = 200;
      nameColumn.HeaderText = "Account Name";
      nameColumn.Frozen = false;
      nameColumn.ValueType = typeof(string);
      dataGridViewAccounts.Columns.Add(nameColumn);

      DataGridViewTextBoxColumn descriptionColumn = new DataGridViewTextBoxColumn();
      descriptionColumn.DataPropertyName = "Description";
      descriptionColumn.Width = 300;
      descriptionColumn.HeaderText = "Description";
      descriptionColumn.ReadOnly = true;
      descriptionColumn.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
      dataGridViewAccounts.Columns.Add(descriptionColumn);

      DataGridViewComboBoxColumn typeColumn = new DataGridViewComboBoxColumn();
      typeColumn.HeaderText = "Account Type";
      typeColumn.DataSource = new Account().getAccountTypes();
      //typeColumn.DisplayMember = "Name";
      //typeColumn.ValueMember = "EmployeeID";
      typeColumn.DataPropertyName = "Type";
      dataGridViewAccounts.Columns.Add(typeColumn);

      // Define user capabilities
      dataGridViewAccounts.AllowUserToAddRows = false;
      dataGridViewAccounts.AllowUserToDeleteRows = false;
    }

    private DataSet loadAllAccounts()
    {
      XmlDataDocument xmlDatadoc = new XmlDataDocument();
      xmlDatadoc.DataSet.ReadXml(filePath);
      DataSet ds = new DataSet("Accounts DataSet");
      ds = xmlDatadoc.DataSet;

      return ds;
    }

    public void saveInXmlFile(int index)
    {
      //Create FileStream fs
      FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

      //Create new XmlDocument
      XmlDocument xmldoc = new XmlDocument();

      //Load the contents of the filestream into the XmlDocument (xmldoc)
      xmldoc.Load(fs);

      //close the fs filestream
      fs.Close();

      //Change the contents of the attribute
      xmldoc.DocumentElement.ChildNodes[index].ChildNodes[0].InnerText = tbxAccountCode.Text;
      xmldoc.DocumentElement.ChildNodes[index].ChildNodes[1].InnerText = tbxAccountName.Text;
      xmldoc.DocumentElement.ChildNodes[index].ChildNodes[2].InnerText = rtbDescription.Text;
      xmldoc.DocumentElement.ChildNodes[index].ChildNodes[3].InnerText = cbxAccountType.Text;
      //.Attributes[0].InnerText = "002";

      // Create the filestream for saving
      FileStream WRITER = new FileStream(filePath, FileMode.Truncate, FileAccess.Write, FileShare.ReadWrite);

      // Save the xmldocument
      xmldoc.Save(WRITER);

      //Close the writer filestream
      WRITER.Close();

      //Update the contents of the listbox
      //WorkingXMLFiles2_Load(new object(), new EventArgs());
      dataGridViewAccounts.Update();
      loadDataGridView();
    }

    private void loadDataGridView()
    {
      ds = loadAllAccounts();
      dataGridViewAccounts.DataSource = ds;
      setupColumns();
      dataGridViewAccounts.DataMember = "Account";
    }
  }
}