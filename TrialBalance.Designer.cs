namespace Accounting.View
{
  partial class frmTrialBalance
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
      this.dataGridViewTrialBalance = new System.Windows.Forms.DataGridView();
      this.AccountCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.AccountName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Debit = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Credit = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.label8 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.lblDateTime = new System.Windows.Forms.Label();
      this.btnPrint = new System.Windows.Forms.Button();
      this.btnClose = new System.Windows.Forms.Button();
      this.btnDisplay = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTrialBalance)).BeginInit();
      this.SuspendLayout();
      // 
      // dataGridViewTrialBalance
      // 
      dataGridViewCellStyle4.BackColor = System.Drawing.Color.PaleGreen;
      dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Red;
      this.dataGridViewTrialBalance.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
      dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dataGridViewTrialBalance.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
      this.dataGridViewTrialBalance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridViewTrialBalance.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.AccountCode,
            this.AccountName,
            this.Debit,
            this.Credit});
      dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
      dataGridViewCellStyle6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
      dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dataGridViewTrialBalance.DefaultCellStyle = dataGridViewCellStyle6;
      this.dataGridViewTrialBalance.Location = new System.Drawing.Point(12, 83);
      this.dataGridViewTrialBalance.Name = "dataGridViewTrialBalance";
      this.dataGridViewTrialBalance.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.dataGridViewTrialBalance.RowTemplate.Height = 30;
      this.dataGridViewTrialBalance.Size = new System.Drawing.Size(689, 365);
      this.dataGridViewTrialBalance.TabIndex = 0;
      // 
      // AccountCode
      // 
      this.AccountCode.HeaderText = "Code";
      this.AccountCode.Name = "AccountCode";
      // 
      // AccountName
      // 
      this.AccountName.HeaderText = "Account Name";
      this.AccountName.Name = "AccountName";
      this.AccountName.Width = 200;
      // 
      // Debit
      // 
      this.Debit.HeaderText = "Debit";
      this.Debit.Name = "Debit";
      // 
      // Credit
      // 
      this.Credit.HeaderText = "Credit";
      this.Credit.Name = "Credit";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label8.ForeColor = System.Drawing.Color.Blue;
      this.label8.Location = new System.Drawing.Point(272, 37);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(168, 24);
      this.label8.TabIndex = 14;
      this.label8.Text = "TRIAL BALANCE";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label5.ForeColor = System.Drawing.Color.Red;
      this.label5.Location = new System.Drawing.Point(9, 9);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(90, 16);
      this.label5.TabIndex = 15;
      this.label5.Text = "XYZ Co., Ltd.";
      // 
      // lblDateTime
      // 
      this.lblDateTime.AutoSize = true;
      this.lblDateTime.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblDateTime.ForeColor = System.Drawing.Color.Blue;
      this.lblDateTime.Location = new System.Drawing.Point(265, 61);
      this.lblDateTime.Name = "lblDateTime";
      this.lblDateTime.Size = new System.Drawing.Size(182, 16);
      this.lblDateTime.TabIndex = 16;
      this.lblDateTime.Text = "Account Name and Description";
      this.lblDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnPrint
      // 
      this.btnPrint.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnPrint.Location = new System.Drawing.Point(321, 457);
      this.btnPrint.Name = "btnPrint";
      this.btnPrint.Size = new System.Drawing.Size(75, 31);
      this.btnPrint.TabIndex = 17;
      this.btnPrint.Text = "&Print...";
      this.btnPrint.UseVisualStyleBackColor = true;
      this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
      // 
      // btnClose
      // 
      this.btnClose.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnClose.Location = new System.Drawing.Point(428, 457);
      this.btnClose.Name = "btnClose";
      this.btnClose.Size = new System.Drawing.Size(75, 31);
      this.btnClose.TabIndex = 17;
      this.btnClose.Text = "&Close";
      this.btnClose.UseVisualStyleBackColor = true;
      this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
      // 
      // btnDisplay
      // 
      this.btnDisplay.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnDisplay.Location = new System.Drawing.Point(209, 457);
      this.btnDisplay.Name = "btnDisplay";
      this.btnDisplay.Size = new System.Drawing.Size(75, 31);
      this.btnDisplay.TabIndex = 17;
      this.btnDisplay.Text = "&Display";
      this.btnDisplay.UseVisualStyleBackColor = true;
      this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
      // 
      // frmTrialBalance
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(713, 496);
      this.Controls.Add(this.btnClose);
      this.Controls.Add(this.btnDisplay);
      this.Controls.Add(this.btnPrint);
      this.Controls.Add(this.lblDateTime);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.label8);
      this.Controls.Add(this.dataGridViewTrialBalance);
      this.Name = "frmTrialBalance";
      this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Trial Balance";
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTrialBalance)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DataGridView dataGridViewTrialBalance;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label lblDateTime;
    private System.Windows.Forms.Button btnPrint;
    private System.Windows.Forms.Button btnClose;
    private System.Windows.Forms.DataGridViewTextBoxColumn AccountCode;
    private System.Windows.Forms.DataGridViewTextBoxColumn AccountName;
    private System.Windows.Forms.DataGridViewTextBoxColumn Debit;
    private System.Windows.Forms.DataGridViewTextBoxColumn Credit;
    private System.Windows.Forms.Button btnDisplay;

  }
}