using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using Accounting.Model;

namespace Accounting.Data
{
  public interface IDataBase
  {
    void Test();
  }

  public class DBConfig : IDataBase
  {
    public const string ACCOUNTS_FILE_NAME = "\\Data\\AllAccounts.xml";
    public const string GEN_JOURNAL_FILE_NAME = "\\Data\\GeneralJournal.xml";

    void IDataBase.Test()
    {
      Console.WriteLine("Test");
    }
  }

  public class XmlStorage
  {
    private string filename;

    public XmlStorage(string filename)
    {
      this.filename = filename;
    }

    public List<Account> loadAllAccounts()
    {
      StreamReader sr = new StreamReader(filename);
      XmlTextReader xr = new XmlTextReader(sr);
      XmlDocument xd = new XmlDocument();
      xd.Load(xr);

      XmlNodeList accounts = xd.SelectNodes("accounts/account");
      List<Account> allaccounts = new List<Account>();
      foreach (XmlNode account in accounts)
      {
        string s1 = account.ChildNodes.Item(0).InnerText;
        string s2 = account.ChildNodes.Item(1).InnerText;
        string s3 = account.ChildNodes.Item(2).InnerText;
        string s4 = account.ChildNodes.Item(3).InnerText;
        Account anaccount = new Account(Int32.Parse(s1), s2, s3, s4);
        allaccounts.Add(anaccount);
      }
      return allaccounts;
    }

    public List<JournalEntry> loadAllJournalEntries()
    {
      StreamReader sr = new StreamReader(filename);
      XmlTextReader xr = new XmlTextReader(sr);
      XmlDocument xd = new XmlDocument();
      xd.Load(xr);

      XmlNodeList entries = xd.SelectNodes("journal/entry");
      List<JournalEntry> allentries = new List<JournalEntry>();

      foreach (XmlNode entry in entries)
      {
        string dAccountName = entry.ChildNodes.Item(0).InnerText;
        string dAccountCode = entry.ChildNodes.Item(0).Attributes.Item(0).InnerText;
        string cAccountName = entry.ChildNodes.Item(1).InnerText;
        string cAccountCode = entry.ChildNodes.Item(1).Attributes.Item(0).InnerText;
        string desc = entry.ChildNodes.Item(2).InnerText;
        string date = entry.ChildNodes.Item(3).InnerText;
        date = date.Substring(3, 2) + "/" + date.Substring(0, 2) + "/" + date.Substring(6, 2);
        string amount = entry.ChildNodes.Item(4).InnerText;

        Account dAcc = new Account(Int32.Parse(dAccountCode), dAccountName, "", "");
        Account cAcc = new Account(Int32.Parse(cAccountCode), cAccountName, "", "");

        JournalEntry ajournalentry = new JournalEntry(dAcc, cAcc, desc, DateTime.Parse(date), Int32.Parse(amount));
        allentries.Add(ajournalentry);
      }
      return allentries;
    }

    public void saveInXmlFile() { }

  }
}
