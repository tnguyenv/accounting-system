using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Accounting.Controller
{
  class System
  {
  }

  public class ApplicationEnvironment
  {
    public string getApplicationDirectory()
    {
      string executableName = Application.ExecutablePath;
      FileInfo executableFileInfo = new FileInfo(executableName);
      string executableDirectoryName = executableFileInfo.DirectoryName;

      string parentName = executableFileInfo.Directory.Parent.FullName;
      parentName = executableFileInfo.Directory.Parent.Parent.FullName;

      return parentName;
    }
  }
}
