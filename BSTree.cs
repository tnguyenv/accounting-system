using System;
using System.Collections.Generic;
using System.Text;

namespace Accounting.Business
{
  public class BSTNode<T>
  {
    private DateTime name;
    public DateTime Name
    {
      get { return this.name; }
      set { this.name = value; }
    }

    private List<T> value;
    public List<T> Value
    {
      get { return this.value; }
      set { this.value = value; }
    }

    public BSTNode<T> Left;
    public BSTNode<T> Right;

    public BSTNode(DateTime name, List<T> value)
    {
      this.name = name;
      this.value = value;
      this.Left = null;
      this.Right = null;
    }

    public override string ToString()
    {
      return name.ToString("dd/MM/yyyy");
    }
  }

  public class BSTree<T>
  {
    const int WIDTH1 = 15;
    const int WIDTH2 = 10;

    private BSTNode<T> root;

    public BSTNode<T> Root
    {
      get { return root; }
      set { root = value; }
    }
    private List<BSTNode<T>> nodes;

    public BSTree()
    {
      root = null;
      nodes = new List<BSTNode<T>>();
    }

    private void killTree(ref BSTNode<T> node)
    {
      if (root != null)
      {
        killTree(ref node.Left);

        killTree(ref node.Right);
        node = null;
      }
    }

    public void clear()
    {
      killTree(ref root);
      nodes.Clear();
    }

    public int count()
    {
      return nodes.Count;
    }

    public BSTNode<T> getnthNode(int n)
    {
      if ((n < 0) || (n >= nodes.Count))
        throw new Exception("Error: Index out of range in getnthNode (BinarySTree)");
      return (BSTNode<T>)nodes[n];
    }

    public void setnthNode(int n, List<T> value)
    {
      if ((n < 0) || (n >= nodes.Count))
        throw new Exception("Error: Index out of range in setnthSymbol (BinarySTree)");
      ((BSTNode<T>)nodes[n]).Value = value;
    }

    private List<T> getValue(DateTime name)
    {
      BSTNode<T> np = findSymbol(name);    // np points to node in tree holding name 
      if (np != null)
        return np.Value;   // return value of name given the pointer 
      else
        throw new Exception("Error: Can''t locate symbol <" + name + ">");
    }

    private BSTNode<T> findSymbol(DateTime name)
    {
      BSTNode<T> np = root;

      int cmp;
      while (np != null)
      {
        cmp = DateTime.Compare(name, np.Name);
        if (cmp == 0)   // found !
          return np;

        if (cmp < 0)
          np = np.Left;
        else
          np = np.Right;
      }
      return null;  // Return null to indicate failure to find name
    }

    // Recursively locates an empty slot in the binary tree and inserts the node
    public void add(BSTNode<T> node, ref BSTNode<T> tree)
    {
      if (tree == null)
      {
        tree = node;
      }
      else
      {
        // If we find a node with the same name then it's 
        // a duplicate and we need only to add its values to the current node
        int comparison = DateTime.Compare(node.Name, tree.Name);

        if (comparison == 0)
        {
          // Add the values to the current node
          foreach (T t in node.Value)
            tree.Value.Add(t);
        }
        else

          if (comparison < 0)
          {
            add(node, ref tree.Left);
          }
          else
          {
            add(node, ref tree.Right);
          }
      }
      nodes.Add(node);
    }

    public BSTNode<T> insert(BSTNode<T> node)
    {
      try
      {
        if (root == null)
          root = node;
        else
          add(node, ref root);

        return node;
      }
      catch (Exception)
      {
        return null;
      }
    }

    public BSTNode<T> insert(DateTime name, List<T> value)
    {
      BSTNode<T> node = new BSTNode<T>(name, value);
      return insert(node);
    }

    private BSTNode<T> findParent(DateTime name, ref BSTNode<T> parent)
    {
      BSTNode<T> np = root;
      parent = null;
      int cmp;
      while (np != null)
      {
        cmp = DateTime.Compare(name, np.Name);
        if (cmp == 0)   // found !
          return np;

        if (cmp < 0)
        {
          parent = np;
          np = np.Left;
        }
        else
        {
          parent = np;
          np = np.Right;
        }
      }
      return null;  // Return null to indicate failure to find name
    }

    public BSTNode<T> findSuccessor(BSTNode<T> startNode, ref BSTNode<T> parent)
    {
      parent = startNode;
      // Look for the left-most node on the right side
      startNode = startNode.Right;
      while (startNode.Left != null)
      {
        parent = startNode;
        startNode = startNode.Left;
      }
      return startNode;
    }

    public void PreOder(BSTNode<T> root, ref List<string> result)
    {
      if (root != null)
      {
        PreOder(root.Left, ref result);
        // get and concat the name of each node
        string s = root.ToString();

        s = s.PadRight(WIDTH1);
        string s1 = root.Value[0].ToString().PadLeft(WIDTH2);
        s += s1;
        result.Add(s);

        for (int i = 1; i < root.Value.Count; i++)
        {
          string ss = "";
          ss += root.Value[i].ToString();
          ss = ss.PadLeft(s.Length);
          result.Add(ss);
        }
        PreOder(root.Right, ref result);
      }
    }
  }
}
