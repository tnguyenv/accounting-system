namespace Accounting.View
{
  partial class frmAccounts
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
      this.dataGridViewAccounts = new System.Windows.Forms.DataGridView();
      this.tbxAccountCode = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.tbxAccountName = new System.Windows.Forms.TextBox();
      this.rtbDescription = new System.Windows.Forms.RichTextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.btnSave = new System.Windows.Forms.Button();
      this.cbxAccountType = new System.Windows.Forms.ComboBox();
      this.label4 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.btnNew = new System.Windows.Forms.Button();
      this.btnReset = new System.Windows.Forms.Button();
      this.btnDelete = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAccounts)).BeginInit();
      this.SuspendLayout();
      // 
      // dataGridViewAccounts
      // 
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
      this.dataGridViewAccounts.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
      dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle2.Format = "d";
      dataGridViewCellStyle2.NullValue = null;
      dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dataGridViewAccounts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
      this.dataGridViewAccounts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
      dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
      dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
      this.dataGridViewAccounts.DefaultCellStyle = dataGridViewCellStyle3;
      this.dataGridViewAccounts.Location = new System.Drawing.Point(12, 53);
      this.dataGridViewAccounts.Name = "dataGridViewAccounts";
      dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dataGridViewAccounts.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
      this.dataGridViewAccounts.RowTemplate.Height = 30;
      this.dataGridViewAccounts.Size = new System.Drawing.Size(802, 412);
      this.dataGridViewAccounts.TabIndex = 0;
      this.dataGridViewAccounts.DoubleClick += new System.EventHandler(this.dataGridViewAccounts_DoubleClick);
      // 
      // tbxAccountCode
      // 
      this.tbxAccountCode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxAccountCode.Location = new System.Drawing.Point(916, 50);
      this.tbxAccountCode.Name = "tbxAccountCode";
      this.tbxAccountCode.Size = new System.Drawing.Size(100, 23);
      this.tbxAccountCode.TabIndex = 2;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(820, 53);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(86, 16);
      this.label1.TabIndex = 3;
      this.label1.Text = "Account Code";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(820, 89);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(90, 16);
      this.label2.TabIndex = 4;
      this.label2.Text = "Account Name";
      // 
      // tbxAccountName
      // 
      this.tbxAccountName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxAccountName.Location = new System.Drawing.Point(916, 86);
      this.tbxAccountName.Name = "tbxAccountName";
      this.tbxAccountName.Size = new System.Drawing.Size(193, 23);
      this.tbxAccountName.TabIndex = 5;
      // 
      // rtbDescription
      // 
      this.rtbDescription.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.rtbDescription.Location = new System.Drawing.Point(916, 127);
      this.rtbDescription.Name = "rtbDescription";
      this.rtbDescription.Size = new System.Drawing.Size(193, 62);
      this.rtbDescription.TabIndex = 6;
      this.rtbDescription.Text = "";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(820, 127);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(71, 16);
      this.label3.TabIndex = 7;
      this.label3.Text = "Description";
      // 
      // btnSave
      // 
      this.btnSave.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnSave.Location = new System.Drawing.Point(916, 293);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(75, 30);
      this.btnSave.TabIndex = 8;
      this.btnSave.Text = "&Save";
      this.btnSave.UseVisualStyleBackColor = true;
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      // 
      // cbxAccountType
      // 
      this.cbxAccountType.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.cbxAccountType.FormattingEnabled = true;
      this.cbxAccountType.Location = new System.Drawing.Point(916, 200);
      this.cbxAccountType.Name = "cbxAccountType";
      this.cbxAccountType.Size = new System.Drawing.Size(193, 24);
      this.cbxAccountType.TabIndex = 9;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.Location = new System.Drawing.Point(820, 205);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(85, 16);
      this.label4.TabIndex = 3;
      this.label4.Text = "Account Type";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label8.ForeColor = System.Drawing.Color.Blue;
      this.label8.Location = new System.Drawing.Point(347, 9);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(430, 24);
      this.label8.TabIndex = 14;
      this.label8.Text = "LIST OF ALL ACCOUNTS IN THE COMPANY";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // btnNew
      // 
      this.btnNew.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnNew.Location = new System.Drawing.Point(916, 243);
      this.btnNew.Name = "btnNew";
      this.btnNew.Size = new System.Drawing.Size(75, 30);
      this.btnNew.TabIndex = 8;
      this.btnNew.Text = "&New";
      this.btnNew.UseVisualStyleBackColor = true;
      this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
      // 
      // btnReset
      // 
      this.btnReset.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnReset.Location = new System.Drawing.Point(1017, 293);
      this.btnReset.Name = "btnReset";
      this.btnReset.Size = new System.Drawing.Size(75, 30);
      this.btnReset.TabIndex = 8;
      this.btnReset.Text = "&Reset";
      this.btnReset.UseVisualStyleBackColor = true;
      this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
      // 
      // btnDelete
      // 
      this.btnDelete.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnDelete.Location = new System.Drawing.Point(1017, 243);
      this.btnDelete.Name = "btnDelete";
      this.btnDelete.Size = new System.Drawing.Size(75, 30);
      this.btnDelete.TabIndex = 8;
      this.btnDelete.Text = "&Delete";
      this.btnDelete.UseVisualStyleBackColor = true;
      this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
      // 
      // frmAccounts
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1124, 479);
      this.Controls.Add(this.label8);
      this.Controls.Add(this.cbxAccountType);
      this.Controls.Add(this.btnReset);
      this.Controls.Add(this.btnDelete);
      this.Controls.Add(this.btnNew);
      this.Controls.Add(this.btnSave);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.rtbDescription);
      this.Controls.Add(this.tbxAccountName);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.tbxAccountCode);
      this.Controls.Add(this.dataGridViewAccounts);
      this.MaximizeBox = false;
      this.Name = "frmAccounts";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "List Of All Accounts";
      this.Load += new System.EventHandler(this.frmAccounts_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAccounts)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DataGridView dataGridViewAccounts;
    private System.Windows.Forms.TextBox tbxAccountCode;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox tbxAccountName;
    private System.Windows.Forms.RichTextBox rtbDescription;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.ComboBox cbxAccountType;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Button btnNew;
    private System.Windows.Forms.Button btnReset;
    private System.Windows.Forms.Button btnDelete;
  }
}