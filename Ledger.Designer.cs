namespace Accounting.View
{
  partial class frmLedger
  {
    private System.ComponentModel.IContainer components = null;
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code
    private void InitializeComponent()
    {
      this.dataGridView1 = new System.Windows.Forms.DataGridView();
      this.cbxAccount = new System.Windows.Forms.ComboBox();
      this.label1 = new System.Windows.Forms.Label();
      this.listBox1 = new System.Windows.Forms.ListBox();
      this.listBox2 = new System.Windows.Forms.ListBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.lblAccountCode = new System.Windows.Forms.Label();
      this.label13 = new System.Windows.Forms.Label();
      this.lblDebitRemain = new System.Windows.Forms.Label();
      this.lblCreditRemain = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
      this.SuspendLayout();
      // 
      // dataGridView1
      // 
      this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Location = new System.Drawing.Point(374, 88);
      this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.Size = new System.Drawing.Size(349, 185);
      this.dataGridView1.TabIndex = 0;
      // 
      // cbxAccount
      // 
      this.cbxAccount.FormattingEnabled = true;
      this.cbxAccount.Location = new System.Drawing.Point(374, 52);
      this.cbxAccount.Name = "cbxAccount";
      this.cbxAccount.Size = new System.Drawing.Size(349, 24);
      this.cbxAccount.TabIndex = 3;
      this.cbxAccount.SelectedIndexChanged += new System.EventHandler(this.cbxAccount_SelectedIndexChanged);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(446, 18);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(155, 16);
      this.label1.TabIndex = 4;
      this.label1.Text = "Please choose an account";
      // 
      // listBox1
      // 
      this.listBox1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.listBox1.FormattingEnabled = true;
      this.listBox1.ItemHeight = 16;
      this.listBox1.Location = new System.Drawing.Point(11, 88);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new System.Drawing.Size(151, 96);
      this.listBox1.TabIndex = 5;
      // 
      // listBox2
      // 
      this.listBox2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
      this.listBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.listBox2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.listBox2.FormattingEnabled = true;
      this.listBox2.ItemHeight = 16;
      this.listBox2.Location = new System.Drawing.Point(178, 88);
      this.listBox2.Name = "listBox2";
      this.listBox2.Size = new System.Drawing.Size(155, 96);
      this.listBox2.TabIndex = 5;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.ForeColor = System.Drawing.Color.Red;
      this.label2.Location = new System.Drawing.Point(80, 40);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(176, 16);
      this.label2.TabIndex = 4;
      this.label2.Text = "Please choose an account";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label6.ForeColor = System.Drawing.Color.Blue;
      this.label6.Location = new System.Drawing.Point(256, 69);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(42, 16);
      this.label6.TabIndex = 7;
      this.label6.Text = "Credit";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.ForeColor = System.Drawing.Color.Blue;
      this.label3.Location = new System.Drawing.Point(42, 69);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(37, 16);
      this.label3.TabIndex = 6;
      this.label3.Text = "Debit";
      // 
      // lblAccountCode
      // 
      this.lblAccountCode.AutoSize = true;
      this.lblAccountCode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblAccountCode.ForeColor = System.Drawing.Color.Blue;
      this.lblAccountCode.Location = new System.Drawing.Point(149, 69);
      this.lblAccountCode.Name = "lblAccountCode";
      this.lblAccountCode.Size = new System.Drawing.Size(40, 16);
      this.lblAccountCode.TabIndex = 8;
      this.lblAccountCode.Text = "Code";
      this.lblAccountCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label13.ForeColor = System.Drawing.Color.Blue;
      this.label13.Location = new System.Drawing.Point(41, 9);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(195, 24);
      this.label13.TabIndex = 14;
      this.label13.Text = "GENERAL LEDGER";
      this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblDebitRemain
      // 
      this.lblDebitRemain.AutoSize = true;
      this.lblDebitRemain.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblDebitRemain.ForeColor = System.Drawing.Color.Blue;
      this.lblDebitRemain.Location = new System.Drawing.Point(12, 196);
      this.lblDebitRemain.Name = "lblDebitRemain";
      this.lblDebitRemain.Size = new System.Drawing.Size(0, 16);
      this.lblDebitRemain.TabIndex = 6;
      // 
      // lblCreditRemain
      // 
      this.lblCreditRemain.AutoSize = true;
      this.lblCreditRemain.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblCreditRemain.ForeColor = System.Drawing.Color.Blue;
      this.lblCreditRemain.Location = new System.Drawing.Point(228, 196);
      this.lblCreditRemain.Name = "lblCreditRemain";
      this.lblCreditRemain.Size = new System.Drawing.Size(0, 16);
      this.lblCreditRemain.TabIndex = 7;
      // 
      // frmLedger
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(747, 397);
      this.Controls.Add(this.label13);
      this.Controls.Add(this.lblAccountCode);
      this.Controls.Add(this.lblCreditRemain);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.lblDebitRemain);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.listBox2);
      this.Controls.Add(this.listBox1);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.cbxAccount);
      this.Controls.Add(this.dataGridView1);
      this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
      this.Name = "frmLedger";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Ledger";
      this.Load += new System.EventHandler(this.frmLedger_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.DataGridView dataGridView1;
    private System.Windows.Forms.ComboBox cbxAccount;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ListBox listBox1;
    private System.Windows.Forms.ListBox listBox2;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label lblAccountCode;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Label lblDebitRemain;
    private System.Windows.Forms.Label lblCreditRemain;
  }
}