using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using Accounting.Controller;
using Accounting.Data;

namespace Accounting.View
{
  public partial class frmJournal : Form
  {
    private DataSet ds = new DataSet();

    private string filePath = new ApplicationEnvironment().getApplicationDirectory() + DBConfig.GEN_JOURNAL_FILE_NAME;

    public frmJournal(frmMainWindow parent)
    {
      InitializeComponent();
      this.MdiParent = parent;
    }

    private void frmJournal_Load(object sender, EventArgs e)
    {
      customDataGridView();
      loadData();
    }

    private void btnFresh_Click(object sender, EventArgs e)
    {
      dataGridViewJournal.Rows.Clear();
      loadData();
    }

    private void btnEdit_Click(object sender, EventArgs e)
    {
      dataGridViewJournal.AllowUserToAddRows = true;
      dataGridViewJournal.AllowUserToDeleteRows = true;
    }

    private void btnPrint_Click(object sender, EventArgs e)
    {
      MessageBox.Show("Sorry! This functionality hasn't constructed yet.", "Accounting System", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      MessageBox.Show("Please refer to the SaveInXMLFile() method in Account.cs", "Accounting System", MessageBoxButtons.OK, MessageBoxIcon.Information);
    }

    private void customDataGridView()
    {
      // (1) Define style for rows
      dataGridViewJournal.RowTemplate.Height = 30;

      // (2) Define style for data cells
      DataGridViewCellStyle style = new DataGridViewCellStyle();
      style.BackColor = Color.Bisque;
      style.Font = new Font("Tahoma", 10, FontStyle.Bold);
      style.ForeColor = Color.Navy;

      // (left,top,right,bottom)
      style.Padding = new Padding(5, 2, 5, 5);
      style.SelectionBackColor = Color.LightBlue;
      dataGridViewJournal.DefaultCellStyle = style;

      // (3) Define style for column headers
      DataGridViewCellStyle styleHdr = new DataGridViewCellStyle();
      styleHdr.Padding = new Padding(1, 1, 1, 1);
      styleHdr.BackColor = Color.OldLace;
      styleHdr.ForeColor = Color.Black;
      dataGridViewJournal.ColumnHeadersDefaultCellStyle = styleHdr;

      // (4) Define user capabilities
      dataGridViewJournal.AllowUserToAddRows = false;
      dataGridViewJournal.AllowUserToOrderColumns = false;
      dataGridViewJournal.AllowUserToDeleteRows = false;
    }

    private void loadData()
    {
      // (5) Place data in grid manually (datasource is better)
      // Load data from XML file
      StreamReader sr = new StreamReader(filePath);
      XmlTextReader xr = new XmlTextReader(sr);
      XmlDocument xmlGeneralJournal = new XmlDocument();
      xmlGeneralJournal.Load(xr);
      XmlNodeList journalEntries = xmlGeneralJournal.SelectNodes("journal/entry");

      for (int i = 0; i < journalEntries.Count; i++)
      {
        string dAccountName = journalEntries.Item(i).ChildNodes.Item(0).InnerText;
        string dAccountCode = journalEntries.Item(i).ChildNodes.Item(0).Attributes.Item(0).InnerText;
        string cAccountName = journalEntries.Item(i).ChildNodes.Item(1).InnerText;
        string cAccountCode = journalEntries.Item(i).ChildNodes.Item(1).Attributes.Item(0).InnerText;
        string desc = journalEntries.Item(i).ChildNodes.Item(2).InnerText;
        string date = journalEntries.Item(i).ChildNodes.Item(3).InnerText;
        string amount = journalEntries.Item(i).ChildNodes.Item(4).InnerText;

        object[] row1 = { date, dAccountName, dAccountCode, amount, null };
        object[] row2 = { null, cAccountName, cAccountCode, null, amount };
        object[] row3 = { null, desc, null, null, null };
        dataGridViewJournal.Rows.Add(row1);
        dataGridViewJournal.Rows.Add(row2);
        dataGridViewJournal.Rows.Add(row3);
      }
    }
  }
}