using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Accounting.Controller;
using Accounting.Data;
using Accounting.Model;
using Accounting.Business;

namespace Accounting.View
{
  public partial class frmTrialBalance : Form
  {
    string filePath = new ApplicationEnvironment().getApplicationDirectory() + DBConfig.GEN_JOURNAL_FILE_NAME;
    public frmTrialBalance(frmMainWindow parent)
    {
      InitializeComponent();
      this.MdiParent = parent;
    }

    private void TrialBalance_Load(object sender, EventArgs e)
    {
      customDataGridView();
      lblDateTime.Text = DateTime.Today.ToString("dd/MM/yyyy");
    }

    private void btnClose_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void btnDisplay_Click(object sender, EventArgs e)
    {
      Set trialBalance = new Set();
      Int32 debitTotal = 0;
      Int32 creditTotal = 0;

      // (1) Load all of entries
      XmlStorage storage = new XmlStorage(filePath);
      List<JournalEntry> entries = storage.loadAllJournalEntries();

      // (2) Create a set of account balance
      foreach (JournalEntry entry in entries)
      {
        AccountBalance abCredit = new AccountBalance();
        AccountBalance abDebit = new AccountBalance();

        abCredit.BAccount = entry.CreditedAccount;
        abCredit.CreditAmount = entry.Amount;
        abCredit.DebitAmount = 0;

        abDebit.BAccount = entry.DebitedAccount;
        abDebit.CreditAmount = 0;
        abDebit.DebitAmount = entry.Amount;

        trialBalance = trialBalance + abCredit;
        trialBalance = trialBalance + abDebit;
      }
      // (3) Scan the set of entry and create rows to add into datagridview
      for (int i = 0; i < trialBalance.Length; i++)
      {
        // Create a row
        debitTotal += trialBalance[i].CreditAmount;
        creditTotal += trialBalance[i].DebitAmount;
        object[] row = {trialBalance[i].BAccount.Code.ToString(), trialBalance[i].BAccount.AccountName,
                                (trialBalance[i].DebitAmount > 0)?trialBalance[i].DebitAmount.ToString():"",
                                (trialBalance[i].CreditAmount > 0)?trialBalance[i].CreditAmount.ToString():""};
        // Attach to datagridview
        dataGridViewTrialBalance.Rows.Add(row);
      }

      // Add the last row contains the total of amount
      string debit = debitTotal.ToString();
      string credit = creditTotal.ToString();
      object[] row2 = { null, null, debit, credit };
      dataGridViewTrialBalance.Rows.Add(row2);

      btnDisplay.Enabled = false;
    }

    private void btnPrint_Click(object sender, EventArgs e)
    {
      TrialBalanceCrystalReport trialBalanceReport = new TrialBalanceCrystalReport();
      trialBalanceReport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, new ApplicationEnvironment().getApplicationDirectory() + "\\Data\\TrialBalance.pdf");
    }

    private void customDataGridView()
    {
      // (1) Define style for rows
      // (2) Define style for data cells
      DataGridViewCellStyle style = new DataGridViewCellStyle();
      style.BackColor = Color.Bisque;
      style.Font = new Font("Tahoma", 10, FontStyle.Bold);
      style.ForeColor = Color.Navy;

      // (left,top,right,bottom)
      style.Padding = new Padding(5, 2, 5, 5);
      style.SelectionBackColor = Color.LightBlue;
      dataGridViewTrialBalance.DefaultCellStyle = style;

      // (3) Define style for column headers
      DataGridViewCellStyle styleHdr = new DataGridViewCellStyle();
      styleHdr.Padding = new Padding(1, 1, 1, 1);
      styleHdr.BackColor = Color.OldLace;
      styleHdr.ForeColor = Color.Black;
      styleHdr.Font = new Font("Times New Roman", 12, FontStyle.Bold);
      dataGridViewTrialBalance.ColumnHeadersDefaultCellStyle = styleHdr;

      // (4) Define user capabilities
      dataGridViewTrialBalance.AllowUserToAddRows = false;
      dataGridViewTrialBalance.AllowUserToOrderColumns = false;
      dataGridViewTrialBalance.AllowUserToDeleteRows = false;
    }
  }
}