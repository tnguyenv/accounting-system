using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Accounting.View;

namespace Accounting.View
{
  public partial class frmMainWindow : Form
  {
    private int childFormNumber = 0;

    public frmMainWindow()
    {
      InitializeComponent();

    }

    private void showNewForm(object sender, EventArgs e)
    {
      // Create a new instance of the child form.
      Form childForm = new Form();
      // Make it a child of this MDI form before showing it.
      childForm.MdiParent = this;
      childForm.Text = "Window " + childFormNumber++;
      childForm.Show();
    }

    private void openFile(object sender, EventArgs e)
    {
      OpenFileDialog openFileDialog = new OpenFileDialog();
      openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
      openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
      if (openFileDialog.ShowDialog(this) == DialogResult.OK)
      {
        string FileName = openFileDialog.FileName;
        // TODO: Add code here to open the file.
      }
    }

    private void SettingToolStripMenuItem_Click(object sender, EventArgs e)
    {
      frmSetting setting = new frmSetting(this);
      setting.Show();
    }

    private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }

    private void CutToolStripMenuItem_Click(object sender, EventArgs e)
    {
      // TODO: Use System.Windows.Forms.Clipboard to insert the selected text or images into the clipboard
    }

    private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
    {
      // TODO: Use System.Windows.Forms.Clipboard to insert the selected text or images into the clipboard
    }

    private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
    {
      // TODO: Use System.Windows.Forms.Clipboard.GetText() or System.Windows.Forms.GetData to retrieve information from the clipboard.
    }

    private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
    {
      toolStrip.Visible = toolBarToolStripMenuItem.Checked;
    }

    private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
    {
      statusStrip.Visible = statusBarToolStripMenuItem.Checked;
    }

    private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
    {
      LayoutMdi(MdiLayout.Cascade);
    }

    private void TileVerticleToolStripMenuItem_Click(object sender, EventArgs e)
    {
      LayoutMdi(MdiLayout.TileVertical);
    }

    private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
    {
      LayoutMdi(MdiLayout.TileHorizontal);
    }

    private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      LayoutMdi(MdiLayout.ArrangeIcons);
    }

    private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
    {
      foreach (Form childForm in MdiChildren)
      {
        childForm.Close();
      }
    }

    private void showUpdate(object sender, EventArgs e)
    {
      frmTransactionRecord fr1 = new frmTransactionRecord(this);
      fr1.Show();
    }

    private void showJournal(object sender, EventArgs e)
    {
      frmJournal fr1 = new frmJournal(this);
      fr1.Show();
    }

    private void showLedger(object sender, EventArgs e)
    {
      frmLedger fr = new frmLedger(this);
      fr.Show();
    }

    private void showTrialBalance(object sender, EventArgs e)
    {
      frmTrialBalance fr = new frmTrialBalance(this);
      fr.Show();
    }

    private void showAllAccounts(object sender, EventArgs e)
    {
      frmAccounts fr = new frmAccounts(this);
      fr.Show();
    }

    private void AboutToApplication(object sender, EventArgs e)
    {
      frmAbout frm = new frmAbout();
      frm.ShowDialog();
    }
  }
}
