namespace Accounting.View
{
  partial class frmMainWindow
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMainWindow));
      this.menuStrip = new System.Windows.Forms.MenuStrip();
      this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
      this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
      this.settingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
      this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.printPreviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.printSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
      this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
      this.toolBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.statusBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.accountingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.allAccountsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
      this.journalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
      this.ledgerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
      this.trialBalanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
      this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
      this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStrip = new System.Windows.Forms.ToolStrip();
      this.newToolStripButton = new System.Windows.Forms.ToolStripButton();
      this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
      this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.printToolStripButton = new System.Windows.Forms.ToolStripButton();
      this.printPreviewToolStripButton = new System.Windows.Forms.ToolStripButton();
      this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.helpToolStripButton = new System.Windows.Forms.ToolStripButton();
      this.statusStrip = new System.Windows.Forms.StatusStrip();
      this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
      this.ToolTip = new System.Windows.Forms.ToolTip(this.components);
      this.menuStrip.SuspendLayout();
      this.toolStrip.SuspendLayout();
      this.statusStrip.SuspendLayout();
      this.SuspendLayout();
      // 
      // menuStrip
      // 
      this.menuStrip.AccessibleDescription = null;
      this.menuStrip.AccessibleName = null;
      resources.ApplyResources(this.menuStrip, "menuStrip");
      this.menuStrip.BackgroundImage = null;
      this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.viewMenu,
            this.accountingToolStripMenuItem,
            this.helpMenu});
      this.menuStrip.Name = "menuStrip";
      this.ToolTip.SetToolTip(this.menuStrip, resources.GetString("menuStrip.ToolTip"));
      // 
      // fileMenu
      // 
      this.fileMenu.AccessibleDescription = null;
      this.fileMenu.AccessibleName = null;
      resources.ApplyResources(this.fileMenu, "fileMenu");
      this.fileMenu.BackgroundImage = null;
      this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.toolStripSeparator3,
            this.settingToolStripMenuItem,
            this.toolStripSeparator4,
            this.printToolStripMenuItem,
            this.printPreviewToolStripMenuItem,
            this.printSetupToolStripMenuItem,
            this.toolStripSeparator5,
            this.exitToolStripMenuItem});
      this.fileMenu.Name = "fileMenu";
      this.fileMenu.ShortcutKeyDisplayString = null;
      // 
      // openToolStripMenuItem
      // 
      this.openToolStripMenuItem.AccessibleDescription = null;
      this.openToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.openToolStripMenuItem, "openToolStripMenuItem");
      this.openToolStripMenuItem.BackgroundImage = null;
      this.openToolStripMenuItem.Name = "openToolStripMenuItem";
      this.openToolStripMenuItem.Click += new System.EventHandler(this.openFile);
      // 
      // toolStripSeparator3
      // 
      this.toolStripSeparator3.AccessibleDescription = null;
      this.toolStripSeparator3.AccessibleName = null;
      resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      // 
      // settingToolStripMenuItem
      // 
      this.settingToolStripMenuItem.AccessibleDescription = null;
      this.settingToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.settingToolStripMenuItem, "settingToolStripMenuItem");
      this.settingToolStripMenuItem.BackgroundImage = null;
      this.settingToolStripMenuItem.Name = "settingToolStripMenuItem";
      this.settingToolStripMenuItem.ShortcutKeyDisplayString = null;
      this.settingToolStripMenuItem.Click += new System.EventHandler(this.SettingToolStripMenuItem_Click);
      // 
      // toolStripSeparator4
      // 
      this.toolStripSeparator4.AccessibleDescription = null;
      this.toolStripSeparator4.AccessibleName = null;
      resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
      this.toolStripSeparator4.Name = "toolStripSeparator4";
      // 
      // printToolStripMenuItem
      // 
      this.printToolStripMenuItem.AccessibleDescription = null;
      this.printToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.printToolStripMenuItem, "printToolStripMenuItem");
      this.printToolStripMenuItem.BackgroundImage = null;
      this.printToolStripMenuItem.Name = "printToolStripMenuItem";
      this.printToolStripMenuItem.ShortcutKeyDisplayString = null;
      // 
      // printPreviewToolStripMenuItem
      // 
      this.printPreviewToolStripMenuItem.AccessibleDescription = null;
      this.printPreviewToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.printPreviewToolStripMenuItem, "printPreviewToolStripMenuItem");
      this.printPreviewToolStripMenuItem.BackgroundImage = null;
      this.printPreviewToolStripMenuItem.Name = "printPreviewToolStripMenuItem";
      this.printPreviewToolStripMenuItem.ShortcutKeyDisplayString = null;
      // 
      // printSetupToolStripMenuItem
      // 
      this.printSetupToolStripMenuItem.AccessibleDescription = null;
      this.printSetupToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.printSetupToolStripMenuItem, "printSetupToolStripMenuItem");
      this.printSetupToolStripMenuItem.BackgroundImage = null;
      this.printSetupToolStripMenuItem.Name = "printSetupToolStripMenuItem";
      this.printSetupToolStripMenuItem.ShortcutKeyDisplayString = null;
      // 
      // toolStripSeparator5
      // 
      this.toolStripSeparator5.AccessibleDescription = null;
      this.toolStripSeparator5.AccessibleName = null;
      resources.ApplyResources(this.toolStripSeparator5, "toolStripSeparator5");
      this.toolStripSeparator5.Name = "toolStripSeparator5";
      // 
      // exitToolStripMenuItem
      // 
      this.exitToolStripMenuItem.AccessibleDescription = null;
      this.exitToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.exitToolStripMenuItem, "exitToolStripMenuItem");
      this.exitToolStripMenuItem.BackgroundImage = null;
      this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
      this.exitToolStripMenuItem.ShortcutKeyDisplayString = null;
      this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolsStripMenuItem_Click);
      // 
      // viewMenu
      // 
      this.viewMenu.AccessibleDescription = null;
      this.viewMenu.AccessibleName = null;
      resources.ApplyResources(this.viewMenu, "viewMenu");
      this.viewMenu.BackgroundImage = null;
      this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBarToolStripMenuItem,
            this.statusBarToolStripMenuItem});
      this.viewMenu.Name = "viewMenu";
      this.viewMenu.ShortcutKeyDisplayString = null;
      // 
      // toolBarToolStripMenuItem
      // 
      this.toolBarToolStripMenuItem.AccessibleDescription = null;
      this.toolBarToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.toolBarToolStripMenuItem, "toolBarToolStripMenuItem");
      this.toolBarToolStripMenuItem.BackgroundImage = null;
      this.toolBarToolStripMenuItem.Checked = true;
      this.toolBarToolStripMenuItem.CheckOnClick = true;
      this.toolBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
      this.toolBarToolStripMenuItem.Name = "toolBarToolStripMenuItem";
      this.toolBarToolStripMenuItem.ShortcutKeyDisplayString = null;
      this.toolBarToolStripMenuItem.Click += new System.EventHandler(this.ToolBarToolStripMenuItem_Click);
      // 
      // statusBarToolStripMenuItem
      // 
      this.statusBarToolStripMenuItem.AccessibleDescription = null;
      this.statusBarToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.statusBarToolStripMenuItem, "statusBarToolStripMenuItem");
      this.statusBarToolStripMenuItem.BackgroundImage = null;
      this.statusBarToolStripMenuItem.Checked = true;
      this.statusBarToolStripMenuItem.CheckOnClick = true;
      this.statusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
      this.statusBarToolStripMenuItem.Name = "statusBarToolStripMenuItem";
      this.statusBarToolStripMenuItem.ShortcutKeyDisplayString = null;
      this.statusBarToolStripMenuItem.Click += new System.EventHandler(this.StatusBarToolStripMenuItem_Click);
      // 
      // accountingToolStripMenuItem
      // 
      this.accountingToolStripMenuItem.AccessibleDescription = null;
      this.accountingToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.accountingToolStripMenuItem, "accountingToolStripMenuItem");
      this.accountingToolStripMenuItem.BackgroundImage = null;
      this.accountingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allAccountsToolStripMenuItem,
            this.toolStripSeparator9,
            this.journalToolStripMenuItem,
            this.toolStripMenuItem1,
            this.ledgerToolStripMenuItem,
            this.toolStripSeparator10,
            this.trialBalanceToolStripMenuItem});
      this.accountingToolStripMenuItem.Name = "accountingToolStripMenuItem";
      this.accountingToolStripMenuItem.ShortcutKeyDisplayString = null;
      // 
      // allAccountsToolStripMenuItem
      // 
      this.allAccountsToolStripMenuItem.AccessibleDescription = null;
      this.allAccountsToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.allAccountsToolStripMenuItem, "allAccountsToolStripMenuItem");
      this.allAccountsToolStripMenuItem.BackgroundImage = null;
      this.allAccountsToolStripMenuItem.Name = "allAccountsToolStripMenuItem";
      this.allAccountsToolStripMenuItem.ShortcutKeyDisplayString = null;
      this.allAccountsToolStripMenuItem.Click += new System.EventHandler(this.showAllAccounts);
      // 
      // toolStripSeparator9
      // 
      this.toolStripSeparator9.AccessibleDescription = null;
      this.toolStripSeparator9.AccessibleName = null;
      resources.ApplyResources(this.toolStripSeparator9, "toolStripSeparator9");
      this.toolStripSeparator9.Name = "toolStripSeparator9";
      // 
      // journalToolStripMenuItem
      // 
      this.journalToolStripMenuItem.AccessibleDescription = null;
      this.journalToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.journalToolStripMenuItem, "journalToolStripMenuItem");
      this.journalToolStripMenuItem.BackgroundImage = null;
      this.journalToolStripMenuItem.Name = "journalToolStripMenuItem";
      this.journalToolStripMenuItem.ShortcutKeyDisplayString = null;
      this.journalToolStripMenuItem.Click += new System.EventHandler(this.showUpdate);
      // 
      // toolStripMenuItem1
      // 
      this.toolStripMenuItem1.AccessibleDescription = null;
      this.toolStripMenuItem1.AccessibleName = null;
      resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
      this.toolStripMenuItem1.BackgroundImage = null;
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      this.toolStripMenuItem1.ShortcutKeyDisplayString = null;
      this.toolStripMenuItem1.Click += new System.EventHandler(this.showJournal);
      // 
      // ledgerToolStripMenuItem
      // 
      this.ledgerToolStripMenuItem.AccessibleDescription = null;
      this.ledgerToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.ledgerToolStripMenuItem, "ledgerToolStripMenuItem");
      this.ledgerToolStripMenuItem.BackgroundImage = null;
      this.ledgerToolStripMenuItem.Name = "ledgerToolStripMenuItem";
      this.ledgerToolStripMenuItem.ShortcutKeyDisplayString = null;
      this.ledgerToolStripMenuItem.Click += new System.EventHandler(this.showLedger);
      // 
      // toolStripSeparator10
      // 
      this.toolStripSeparator10.AccessibleDescription = null;
      this.toolStripSeparator10.AccessibleName = null;
      resources.ApplyResources(this.toolStripSeparator10, "toolStripSeparator10");
      this.toolStripSeparator10.Name = "toolStripSeparator10";
      // 
      // trialBalanceToolStripMenuItem
      // 
      this.trialBalanceToolStripMenuItem.AccessibleDescription = null;
      this.trialBalanceToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.trialBalanceToolStripMenuItem, "trialBalanceToolStripMenuItem");
      this.trialBalanceToolStripMenuItem.BackgroundImage = null;
      this.trialBalanceToolStripMenuItem.Name = "trialBalanceToolStripMenuItem";
      this.trialBalanceToolStripMenuItem.ShortcutKeyDisplayString = null;
      this.trialBalanceToolStripMenuItem.Click += new System.EventHandler(this.showTrialBalance);
      // 
      // helpMenu
      // 
      this.helpMenu.AccessibleDescription = null;
      this.helpMenu.AccessibleName = null;
      resources.ApplyResources(this.helpMenu, "helpMenu");
      this.helpMenu.BackgroundImage = null;
      this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.indexToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.toolStripSeparator8,
            this.aboutToolStripMenuItem});
      this.helpMenu.Name = "helpMenu";
      this.helpMenu.ShortcutKeyDisplayString = null;
      // 
      // contentsToolStripMenuItem
      // 
      this.contentsToolStripMenuItem.AccessibleDescription = null;
      this.contentsToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.contentsToolStripMenuItem, "contentsToolStripMenuItem");
      this.contentsToolStripMenuItem.BackgroundImage = null;
      this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
      this.contentsToolStripMenuItem.ShortcutKeyDisplayString = null;
      // 
      // indexToolStripMenuItem
      // 
      this.indexToolStripMenuItem.AccessibleDescription = null;
      this.indexToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.indexToolStripMenuItem, "indexToolStripMenuItem");
      this.indexToolStripMenuItem.BackgroundImage = null;
      this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
      this.indexToolStripMenuItem.ShortcutKeyDisplayString = null;
      // 
      // searchToolStripMenuItem
      // 
      this.searchToolStripMenuItem.AccessibleDescription = null;
      this.searchToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.searchToolStripMenuItem, "searchToolStripMenuItem");
      this.searchToolStripMenuItem.BackgroundImage = null;
      this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
      this.searchToolStripMenuItem.ShortcutKeyDisplayString = null;
      // 
      // toolStripSeparator8
      // 
      this.toolStripSeparator8.AccessibleDescription = null;
      this.toolStripSeparator8.AccessibleName = null;
      resources.ApplyResources(this.toolStripSeparator8, "toolStripSeparator8");
      this.toolStripSeparator8.Name = "toolStripSeparator8";
      // 
      // aboutToolStripMenuItem
      // 
      this.aboutToolStripMenuItem.AccessibleDescription = null;
      this.aboutToolStripMenuItem.AccessibleName = null;
      resources.ApplyResources(this.aboutToolStripMenuItem, "aboutToolStripMenuItem");
      this.aboutToolStripMenuItem.BackgroundImage = null;
      this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
      this.aboutToolStripMenuItem.ShortcutKeyDisplayString = null;
      this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToApplication);
      // 
      // toolStrip
      // 
      this.toolStrip.AccessibleDescription = null;
      this.toolStrip.AccessibleName = null;
      resources.ApplyResources(this.toolStrip, "toolStrip");
      this.toolStrip.BackgroundImage = null;
      this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripButton,
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator1,
            this.printToolStripButton,
            this.printPreviewToolStripButton,
            this.toolStripSeparator2,
            this.helpToolStripButton});
      this.toolStrip.Name = "toolStrip";
      this.ToolTip.SetToolTip(this.toolStrip, resources.GetString("toolStrip.ToolTip"));
      // 
      // newToolStripButton
      // 
      this.newToolStripButton.AccessibleDescription = null;
      this.newToolStripButton.AccessibleName = null;
      resources.ApplyResources(this.newToolStripButton, "newToolStripButton");
      this.newToolStripButton.BackgroundImage = null;
      this.newToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.newToolStripButton.Name = "newToolStripButton";
      this.newToolStripButton.Click += new System.EventHandler(this.showNewForm);
      // 
      // openToolStripButton
      // 
      this.openToolStripButton.AccessibleDescription = null;
      this.openToolStripButton.AccessibleName = null;
      resources.ApplyResources(this.openToolStripButton, "openToolStripButton");
      this.openToolStripButton.BackgroundImage = null;
      this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.openToolStripButton.Name = "openToolStripButton";
      this.openToolStripButton.Click += new System.EventHandler(this.openFile);
      // 
      // saveToolStripButton
      // 
      this.saveToolStripButton.AccessibleDescription = null;
      this.saveToolStripButton.AccessibleName = null;
      resources.ApplyResources(this.saveToolStripButton, "saveToolStripButton");
      this.saveToolStripButton.BackgroundImage = null;
      this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.saveToolStripButton.Name = "saveToolStripButton";
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.AccessibleDescription = null;
      this.toolStripSeparator1.AccessibleName = null;
      resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      // 
      // printToolStripButton
      // 
      this.printToolStripButton.AccessibleDescription = null;
      this.printToolStripButton.AccessibleName = null;
      resources.ApplyResources(this.printToolStripButton, "printToolStripButton");
      this.printToolStripButton.BackgroundImage = null;
      this.printToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.printToolStripButton.Name = "printToolStripButton";
      // 
      // printPreviewToolStripButton
      // 
      this.printPreviewToolStripButton.AccessibleDescription = null;
      this.printPreviewToolStripButton.AccessibleName = null;
      resources.ApplyResources(this.printPreviewToolStripButton, "printPreviewToolStripButton");
      this.printPreviewToolStripButton.BackgroundImage = null;
      this.printPreviewToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.printPreviewToolStripButton.Name = "printPreviewToolStripButton";
      // 
      // toolStripSeparator2
      // 
      this.toolStripSeparator2.AccessibleDescription = null;
      this.toolStripSeparator2.AccessibleName = null;
      resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      // 
      // helpToolStripButton
      // 
      this.helpToolStripButton.AccessibleDescription = null;
      this.helpToolStripButton.AccessibleName = null;
      resources.ApplyResources(this.helpToolStripButton, "helpToolStripButton");
      this.helpToolStripButton.BackgroundImage = null;
      this.helpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
      this.helpToolStripButton.Name = "helpToolStripButton";
      // 
      // statusStrip
      // 
      this.statusStrip.AccessibleDescription = null;
      this.statusStrip.AccessibleName = null;
      resources.ApplyResources(this.statusStrip, "statusStrip");
      this.statusStrip.BackgroundImage = null;
      this.statusStrip.Font = null;
      this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
      this.statusStrip.Name = "statusStrip";
      this.ToolTip.SetToolTip(this.statusStrip, resources.GetString("statusStrip.ToolTip"));
      // 
      // toolStripStatusLabel
      // 
      this.toolStripStatusLabel.AccessibleDescription = null;
      this.toolStripStatusLabel.AccessibleName = null;
      resources.ApplyResources(this.toolStripStatusLabel, "toolStripStatusLabel");
      this.toolStripStatusLabel.BackgroundImage = null;
      this.toolStripStatusLabel.Name = "toolStripStatusLabel";
      // 
      // frmMainWindow
      // 
      this.AccessibleDescription = null;
      this.AccessibleName = null;
      resources.ApplyResources(this, "$this");
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.White;
      this.BackgroundImage = null;
      this.Controls.Add(this.statusStrip);
      this.Controls.Add(this.toolStrip);
      this.Controls.Add(this.menuStrip);
      this.Font = null;
      this.IsMdiContainer = true;
      this.MainMenuStrip = this.menuStrip;
      this.Name = "frmMainWindow";
      this.ToolTip.SetToolTip(this, resources.GetString("$this.ToolTip"));
      this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
      this.menuStrip.ResumeLayout(false);
      this.menuStrip.PerformLayout();
      this.toolStrip.ResumeLayout(false);
      this.toolStrip.PerformLayout();
      this.statusStrip.ResumeLayout(false);
      this.statusStrip.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }
    #endregion


    private System.Windows.Forms.MenuStrip menuStrip;
    private System.Windows.Forms.ToolStrip toolStrip;
    private System.Windows.Forms.StatusStrip statusStrip;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
    private System.Windows.Forms.ToolStripMenuItem printSetupToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
    private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
    private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem fileMenu;
    private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem printPreviewToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem viewMenu;
    private System.Windows.Forms.ToolStripMenuItem toolBarToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem statusBarToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem helpMenu;
    private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
    private System.Windows.Forms.ToolStripButton newToolStripButton;
    private System.Windows.Forms.ToolStripButton openToolStripButton;
    private System.Windows.Forms.ToolStripButton saveToolStripButton;
    private System.Windows.Forms.ToolStripButton printToolStripButton;
    private System.Windows.Forms.ToolStripButton printPreviewToolStripButton;
    private System.Windows.Forms.ToolStripButton helpToolStripButton;
    private System.Windows.Forms.ToolTip ToolTip;
    private System.Windows.Forms.ToolStripMenuItem accountingToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem allAccountsToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem journalToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem ledgerToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem trialBalanceToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
    private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem settingToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
  }
}
