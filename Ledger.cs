using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Accounting.Business;
using Accounting.Controller;
using Accounting.Model;
using Accounting.Data;

namespace Accounting.View
{
  public partial class frmLedger : Form
  {
    string filePath = new ApplicationEnvironment().getApplicationDirectory();
    DataSet ipDataSet = new DataSet();
    List<JournalEntry> entries = new List<JournalEntry>();
    List<Account> accounts = new List<Account>();

    public frmLedger(frmMainWindow parent)
    {
      InitializeComponent();
      this.MdiParent = parent;
    }

    private void frmLedger_Load(object sender, EventArgs e)
    {
      entries = new XmlStorage(filePath + DBConfig.GEN_JOURNAL_FILE_NAME).loadAllJournalEntries();
      accounts = new XmlStorage(filePath + DBConfig.ACCOUNTS_FILE_NAME).loadAllAccounts();

      // Display an account into DataGridView

      // Add all accounts into combox box
      foreach (Account account in accounts)
        cbxAccount.Items.Add(account.AccountName);
      lblAccountCode.Text = "";
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);

      Graphics g = e.Graphics;
      Pen pn = new Pen(Color.Red);
      //Rectangle rect = new Rectangle(50, 50, 200, 100);

      // Ve T-Account
      // Ve duong nam ngang
      Point pt1 = new Point(35, 85);
      Point pt2 = new Point(300, 85);
      g.DrawLine(pn, pt1, pt2);

      // Ve thanh thang dung
      pt1.X = pt1.X + (int)(pt2.X - pt1.X) / 2;

      pt2.X = pt1.X;
      pt2.Y = pt1.Y + 100;
      g.DrawLine(pn, pt1, pt2);
    }

    private void cbxAccount_SelectedIndexChanged(object sender, EventArgs e)
    {
      label2.Text = cbxAccount.SelectedItem.ToString();
      listBox1.Items.Clear();
      listBox2.Items.Clear();
      lblCreditRemain.Text = "";
      lblDebitRemain.Text = "";
      List<JournalEntry> debitEntries = new List<JournalEntry>();
      List<JournalEntry> creditEntries = new List<JournalEntry>();
      BSTree<int> tree1 = new BSTree<int>();
      BSTree<int> tree2 = new BSTree<int>();

      // Create two binary trees
      foreach (JournalEntry entry in entries)
      {
        if (entry.DebitedAccount.AccountName.Equals(cbxAccount.SelectedItem.ToString()))
        {
          // Add a node in the tree1
          debitEntries.Add(entry);
          List<Int32> l = new List<Int32>();
          l.Add(entry.Amount);
          BSTNode<Int32> node = new BSTNode<Int32>(entry.Date, l);
          tree1.insert(node);
        }
        if (entry.CreditedAccount.AccountName.Equals(cbxAccount.SelectedItem.ToString()))
        {
          // Add a node in the tree2
          creditEntries.Add(entry);
          List<Int32> l = new List<int>();
          l.Add(entry.Amount);
          BSTNode<Int32> node = new BSTNode<Int32>(entry.Date, l);
          tree2.insert(node);
        }
      }

      // and then scan all entries in the tree by date
      List<string> s1 = new List<string>(); tree1.PreOder(tree1.Root, ref s1);
      List<string> s2 = new List<string>(); tree2.PreOder(tree2.Root, ref s2);

      // Debit side
      foreach (string s in s1)
      {
        listBox1.Items.Add(s);
      }

      // Credit side
      foreach (string s in s2)
      {
        listBox2.Items.Add(s);
      }

      // Deal with tempentries
      Int32 debitSum = 0;
      Int32 creditSum = 0;
      Int32 remain = 0;
      foreach (JournalEntry entry in debitEntries)
        debitSum += entry.Amount;
      foreach (JournalEntry entry in creditEntries)
        creditSum += entry.Amount;
      remain = debitSum - creditSum;
      if (remain > 0)
        lblDebitRemain.Text = "Remainer: " + Math.Abs(remain).ToString();
      else
        lblCreditRemain.Text = "Remainer: " + Math.Abs(remain).ToString();
    }
  }
}