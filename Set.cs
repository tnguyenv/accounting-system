/*
C# A Beginner's Guide
By Schildt

Publisher: Osborne McGraw-Hill
ISBN: 0072133295
*/
/* 
   Project 7-1 
 
   A set class for characters.  
*/
using System;
using Accounting.Business;
using Accounting.Model;

namespace Accounting.Business
{
  public class Set
  {
    AccountBalance[] members; // this array holds the set    
    int len; // number of members 

    public int Length
    {
      get { return this.len; }
      set { this.len = value; }
    }

    public Set()
    {
      this.len = 0;
    }

    // Construct an empty set of a given size.   
    public Set(int size)
    {
      this.members = new AccountBalance[size]; // allocate memory for set    
      this.len = 0; // no members when constructed 
    }

    // Construct a set from another set. 
    public Set(Set s)
    {
      this.members = new AccountBalance[s.len]; // allocate memory for set    
      for (int i = 0; i < s.len; i++) this.members[i] = s[i];
      this.len = s.len; // number of members 
    }

    // Implement read-only indexer. 
    public AccountBalance this[int idx]
    {
      get
      {
        try
        {
          if (idx >= 0 & idx < len) return members[idx];
          else
            return null;
        }
        catch (Exception)
        {
          return null;
        }
      }
    }

    /* See if an element is in the set.    
       Return the index of the element 
       or -1 if not found. */
    int find(int code)
    {
      for (int i = 0; i < len; i++)
        if (members[i].BAccount.Code == code) return i;

      return -1;
    }

    // Add a unique element to a set.    
    public static Set operator +(Set ob, AccountBalance account)
    {
      Set newset = new Set(ob.len + 1); // make a new set one element larger 

      // copy elements 
      for (int i = 0; i < ob.len; i++)
        newset.members[i] = ob.members[i];

      // set len 
      newset.len = ob.len;

      // see if element already exists 
      int pos = ob.find(account.BAccount.Code);
      if (pos == -1)
      { // if not found, then add 
        // add new element to new set 
        newset.members[newset.len] = account;
        newset.len++;
      }
      else
      {
        ob[pos].CreditAmount += account.CreditAmount;
        ob[pos].DebitAmount += account.DebitAmount;
      }
      return newset; // return updated set 
    }

    // Remove an element from the set.    
    public static Set operator -(Set ob, AccountBalance account)
    {
      Set newset = new Set();
      int i = ob.find(account.BAccount.Code); // i will be -1 if element not found 

      // copy and compress the remaining elements 
      for (int j = 0; j < ob.len; j++)
        if (j != i) newset = newset + ob.members[j];

      return newset;
    }

    // Set union. 
    public static Set operator +(Set ob1, Set ob2)
    {
      Set newset = new Set(ob1); // copy the first set 

      // add unique elements from second set 
      for (int i = 0; i < ob2.len; i++)
        newset = newset + ob2[i];

      return newset; // return updated set 
    }

    // Set difference. 
    public static Set operator -(Set ob1, Set ob2)
    {
      Set newset = new Set(ob1); // copy the first set 

      // subtract elements from second set 
      for (int i = 0; i < ob2.len; i++)
        newset = newset - ob2[i];

      return newset; // return updated set 
    }
  }
}