using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Accounting.View
{
  public partial class frmSetting : Form
  {
    public frmSetting(frmMainWindow parent)
    {
      InitializeComponent();
      this.MdiParent = parent;
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      Close();
    }

    private void btnBrowse_Click(object sender, EventArgs e)
    {
      FolderBrowserDialog fbDialog = new FolderBrowserDialog();
      if (fbDialog.ShowDialog() == DialogResult.OK)
        tbxDataDirectory.Text = fbDialog.SelectedPath;
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      // Save the new values into config.inf
      MessageBox.Show(Directory.GetCurrentDirectory());
    }
  }
}