using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Accounting.LearnCSharp;
using Accounting.Controller;
using Accounting.View;
using Accounting.Data;

namespace Accounting
{
  static class Program
  {
    [STAThread]
    static void Main(string[] args)
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);

      frmMainWindow fr1 = new frmMainWindow();
      Application.Run(fr1);

      IDataBase t = new DBConfig();
      t.Test();
    }
  }
}