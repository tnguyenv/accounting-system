using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using Accounting.Controller;
using Accounting.Model;
using Accounting.Data;

namespace Accounting.View
{
  public partial class frmTransactionRecord : Form
  {
    private XmlNodeList listAccounts;
    private string filePath = new ApplicationEnvironment().getApplicationDirectory() + DBConfig.ACCOUNTS_FILE_NAME;

    public frmTransactionRecord(frmMainWindow parent)
    {
      InitializeComponent();

      this.MdiParent = parent;
      lblDateTime.Text = DateTime.Today.ToShortDateString();
    }

    private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
    {
      lblDateTime.Text = dateTimePicker1.Text;
      DateTime choseDate = dateTimePicker1.Value;
      tbxYear.Text = choseDate.Year.ToString();
      tbxMonth.Text = choseDate.Month.ToString();
      tbxDay.Text = choseDate.Day.ToString();
    }

    private void frmJournal_Load(object sender, EventArgs e)
    {
      // Load all of accounts form XML file
      StreamReader sr = new StreamReader(new ApplicationEnvironment().getApplicationDirectory() + "\\Data\\AllAccounts.xml");

      XmlTextReader xr = new XmlTextReader(sr);

      XmlDocument myAccountsDoc = new XmlDocument();

      myAccountsDoc.Load(xr);

      listAccounts = myAccountsDoc.SelectNodes("accounts/account");
      // Fill them into comboBox
      cbxCreditAccountName.Items.Add("Please choose an account here");
      cbxDebitAccountName.Items.Add("Please choose an account here");
      tbxDescription.Text = "Please enter description here";

      for (int i = 0; i < listAccounts.Count; i++)
      {
        cbxCreditAccountName.Items.Add(listAccounts.Item(i).ChildNodes.Item(1).InnerText);
        cbxDebitAccountName.Items.Add(listAccounts.Item(i).ChildNodes.Item(1).InnerText);
      }

      cbxCreditAccountName.SelectedIndex = 0;
      cbxDebitAccountName.SelectedIndex = 0;
    }

    private void cbxDebitAccountName_SelectedIndexChanged(object sender, EventArgs e)
    {
      string code = listAccounts.Item(cbxDebitAccountName.SelectedIndex).ChildNodes.Item(0).InnerText;
      tbxDebitAccountCode.Text = code;
      lblDebitAccountName.Text = cbxDebitAccountName.Text;
      lblDebitAccountCode.Text = code;
    }

    private void cbxCreditAccountName_SelectedIndexChanged(object sender, EventArgs e)
    {
      string code = listAccounts.Item(cbxCreditAccountName.SelectedIndex).ChildNodes.Item(0).InnerText;
      tbxCreditAccountCode.Text = code;
      lblCreditAccountName.Text = cbxCreditAccountName.Text;
      lblCreditAccountCode.Text = code;
    }

    private void tbxDebitAmount_TextChanged(object sender, EventArgs e)
    {
      tbxCreditAmount.Text = tbxDebitAmount.Text;
      textBox3.Text = dateTimePicker1.Value.ToString("dd/MM/yyy").PadRight(20) + "  " + tbxDebitAmount.Text;
      if (!btnSave.Enabled)
        btnSave.Enabled = true;
    }

    private void tbxCreditAmount_TextChanged(object sender, EventArgs e)
    {
      tbxDebitAmount.Text = tbxCreditAmount.Text;
      textBox6.Text = dateTimePicker1.Value.ToString("dd/MM/yyy").PadRight(20) + "  " + tbxCreditAmount.Text;
      if (!btnSave.Enabled)
        btnSave.Enabled = true;
    }

    protected override void OnPaint(PaintEventArgs e)
    {
      base.OnPaint(e);

      Graphics g = e.Graphics;
      Pen pn = new Pen(Color.Red);
      //Rectangle rect = new Rectangle(50, 50, 200, 100);

      // Ve 2 duong nam ngang
      Point pt1 = new Point(5, 335);
      Point pt2 = new Point(450, 335);
      g.DrawLine(pn, pt1, pt2);

      pt1.X = 500;
      pt1.Y = 335;

      pt2.X = 945;
      pt2.Y = 335;
      g.DrawLine(pn, pt1, pt2);

      // Ve 2 thanh thang dung
      pt1.X = 228;
      pt1.Y = 335;

      pt2.X = pt1.X;
      pt2.Y = pt1.Y + 50;
      g.DrawLine(pn, pt1, pt2);

      pt1.X = 500 + 225;
      pt1.Y = 335;

      pt2.X = pt1.X;
      pt2.Y = pt1.Y + 50;
      g.DrawLine(pn, pt1, pt2);
    }

    private void btnShowAll_Click(object sender, EventArgs e)
    {
      frmMainWindow parent = (frmMainWindow)this.MdiParent;
      frmJournal frm = new frmJournal(parent);
      frm.Show();
    }

    private void btnReset_Click(object sender, EventArgs e)
    {
      tbxYear.Text = "";
      tbxMonth.Text = "";
      tbxDay.Text = "";

      cbxDebitAccountName.SelectedIndex = 0;
      cbxCreditAccountName.SelectedIndex = 0;
      tbxDescription.Text = "Please enter description here";
      tbxDebitAccountCode.Text = "";
      tbxCreditAccountCode.Text = "";
      tbxDebitAmount.Text = "";
      tbxCreditAmount.Text = "";

      textBox3.Text = ""; textBox4.Text = ""; textBox5.Text = ""; textBox6.Text = "";
    }

    private void btnNew_Click(object sender, EventArgs e)
    {
      btnReset_Click(sender, e);
      tbxYear.Text = DateTime.Now.Year.ToString();
      tbxMonth.Text = DateTime.Now.Month.ToString();
      tbxDay.Text = DateTime.Now.Day.ToString();
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      saveEntry();
    }

    void saveEntry()
    {
      // Get data from the controls
      DateTime dateTime = dateTimePicker1.Value;
      string debitName = cbxDebitAccountName.Text;
      Int32 debitCode = Int32.Parse(tbxDebitAccountCode.Text);
      string creditName = cbxCreditAccountName.Text;
      Int32 creditCode = Int32.Parse(tbxCreditAccountCode.Text);
      Int32 amount = Int32.Parse(tbxDebitAmount.Text);
      string desc = tbxDescription.Text;

      // Create a new entry to store the data
      Account dAccount = new Account(debitCode, debitName, "", "");
      Account cAccount = new Account(creditCode, creditName, "", "");
      JournalEntry entry = new JournalEntry(dAccount, cAccount, desc, dateTime, amount);

      entry.saveInXmlFile(filePath);
      btnSave.Enabled = false;
    }
  }
}