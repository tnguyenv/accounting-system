namespace Accounting.View
{
  partial class frmJournal
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
      this.label8 = new System.Windows.Forms.Label();
      this.dataGridViewJournal = new System.Windows.Forms.DataGridView();
      this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.AccountName = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Code = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Debited = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.Credited = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.btnEdit = new System.Windows.Forms.Button();
      this.btnSave = new System.Windows.Forms.Button();
      this.btnPrint = new System.Windows.Forms.Button();
      this.btnFresh = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewJournal)).BeginInit();
      this.SuspendLayout();
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label8.ForeColor = System.Drawing.Color.Blue;
      this.label8.Location = new System.Drawing.Point(344, 9);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(207, 24);
      this.label8.TabIndex = 14;
      this.label8.Text = "GENERAL JOURNAL";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // dataGridViewJournal
      // 
      dataGridViewCellStyle28.BackColor = System.Drawing.Color.PaleGreen;
      dataGridViewCellStyle28.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle28.ForeColor = System.Drawing.Color.Red;
      this.dataGridViewJournal.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle28;
      dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle29.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle29.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle29.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dataGridViewJournal.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle29;
      this.dataGridViewJournal.ColumnHeadersHeight = 30;
      this.dataGridViewJournal.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date,
            this.AccountName,
            this.Code,
            this.Debited,
            this.Credited});
      this.dataGridViewJournal.Location = new System.Drawing.Point(12, 39);
      this.dataGridViewJournal.Name = "dataGridViewJournal";
      dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
      dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dataGridViewJournal.RowHeadersDefaultCellStyle = dataGridViewCellStyle35;
      dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle36.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.dataGridViewJournal.RowsDefaultCellStyle = dataGridViewCellStyle36;
      this.dataGridViewJournal.RowTemplate.Height = 30;
      this.dataGridViewJournal.Size = new System.Drawing.Size(870, 392);
      this.dataGridViewJournal.TabIndex = 16;
      // 
      // Date
      // 
      dataGridViewCellStyle30.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Date.DefaultCellStyle = dataGridViewCellStyle30;
      this.Date.HeaderText = "Date";
      this.Date.Name = "Date";
      this.Date.Resizable = System.Windows.Forms.DataGridViewTriState.True;
      // 
      // AccountName
      // 
      dataGridViewCellStyle31.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.AccountName.DefaultCellStyle = dataGridViewCellStyle31;
      this.AccountName.HeaderText = "Account Name and Description";
      this.AccountName.Name = "AccountName";
      this.AccountName.Width = 400;
      // 
      // Code
      // 
      dataGridViewCellStyle32.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Code.DefaultCellStyle = dataGridViewCellStyle32;
      this.Code.HeaderText = "Code";
      this.Code.Name = "Code";
      // 
      // Debited
      // 
      dataGridViewCellStyle33.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Debited.DefaultCellStyle = dataGridViewCellStyle33;
      this.Debited.HeaderText = "Debited";
      this.Debited.Name = "Debited";
      // 
      // Credited
      // 
      dataGridViewCellStyle34.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Credited.DefaultCellStyle = dataGridViewCellStyle34;
      this.Credited.HeaderText = "Credited";
      this.Credited.Name = "Credited";
      // 
      // btnEdit
      // 
      this.btnEdit.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnEdit.Location = new System.Drawing.Point(288, 446);
      this.btnEdit.Name = "btnEdit";
      this.btnEdit.Size = new System.Drawing.Size(75, 29);
      this.btnEdit.TabIndex = 17;
      this.btnEdit.Text = "&Edit";
      this.btnEdit.UseVisualStyleBackColor = true;
      this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
      // 
      // btnSave
      // 
      this.btnSave.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnSave.Location = new System.Drawing.Point(369, 446);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(75, 29);
      this.btnSave.TabIndex = 17;
      this.btnSave.Text = "&Save";
      this.btnSave.UseVisualStyleBackColor = true;
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      // 
      // btnPrint
      // 
      this.btnPrint.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnPrint.Location = new System.Drawing.Point(451, 446);
      this.btnPrint.Name = "btnPrint";
      this.btnPrint.Size = new System.Drawing.Size(75, 29);
      this.btnPrint.TabIndex = 17;
      this.btnPrint.Text = "&Print...";
      this.btnPrint.UseVisualStyleBackColor = true;
      this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
      // 
      // btnFresh
      // 
      this.btnFresh.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnFresh.Location = new System.Drawing.Point(532, 446);
      this.btnFresh.Name = "btnFresh";
      this.btnFresh.Size = new System.Drawing.Size(75, 29);
      this.btnFresh.TabIndex = 17;
      this.btnFresh.Text = "&Refresh";
      this.btnFresh.UseVisualStyleBackColor = true;
      this.btnFresh.Click += new System.EventHandler(this.btnFresh_Click);
      // 
      // frmJournal
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(894, 487);
      this.Controls.Add(this.btnFresh);
      this.Controls.Add(this.btnPrint);
      this.Controls.Add(this.btnSave);
      this.Controls.Add(this.btnEdit);
      this.Controls.Add(this.dataGridViewJournal);
      this.Controls.Add(this.label8);
      this.MaximizeBox = false;
      this.Name = "frmJournal";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "General Journal";
      this.Load += new System.EventHandler(this.frmJournal_Load);
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewJournal)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.DataGridView dataGridViewJournal;
    private System.Windows.Forms.Button btnEdit;
    private System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.Button btnPrint;
    private System.Windows.Forms.Button btnFresh;
    private System.Windows.Forms.DataGridViewTextBoxColumn Date;
    private System.Windows.Forms.DataGridViewTextBoxColumn AccountName;
    private System.Windows.Forms.DataGridViewTextBoxColumn Code;
    private System.Windows.Forms.DataGridViewTextBoxColumn Debited;
    private System.Windows.Forms.DataGridViewTextBoxColumn Credited;
  }
}