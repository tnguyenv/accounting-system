namespace Accounting.View
{
  partial class frmTransactionRecord
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label5 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
      this.lblDateTime = new System.Windows.Forms.Label();
      this.tbxMonth = new System.Windows.Forms.TextBox();
      this.tbxDay = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.tbxYear = new System.Windows.Forms.TextBox();
      this.label10 = new System.Windows.Forms.Label();
      this.tbxDebitAccountCode = new System.Windows.Forms.TextBox();
      this.tbxCreditAccountCode = new System.Windows.Forms.TextBox();
      this.label11 = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.tbxDebitAmount = new System.Windows.Forms.TextBox();
      this.tbxCreditAmount = new System.Windows.Forms.TextBox();
      this.btnSave = new System.Windows.Forms.Button();
      this.cbxDebitAccountName = new System.Windows.Forms.ComboBox();
      this.cbxCreditAccountName = new System.Windows.Forms.ComboBox();
      this.label13 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.lblDebitAccountName = new System.Windows.Forms.Label();
      this.lblDebitAccountCode = new System.Windows.Forms.Label();
      this.lblCreditAccountCode = new System.Windows.Forms.Label();
      this.label14 = new System.Windows.Forms.Label();
      this.label15 = new System.Windows.Forms.Label();
      this.lblCreditAccountName = new System.Windows.Forms.Label();
      this.btnShowAll = new System.Windows.Forms.Button();
      this.textBox3 = new System.Windows.Forms.TextBox();
      this.textBox4 = new System.Windows.Forms.TextBox();
      this.textBox5 = new System.Windows.Forms.TextBox();
      this.textBox6 = new System.Windows.Forms.TextBox();
      this.btnNew = new System.Windows.Forms.Button();
      this.btnReset = new System.Windows.Forms.Button();
      this.tbxDescription = new System.Windows.Forms.TextBox();
      this.chkSoDu = new System.Windows.Forms.CheckBox();
      this.SuspendLayout();
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label5.ForeColor = System.Drawing.Color.Blue;
      this.label5.Location = new System.Drawing.Point(253, 124);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(182, 16);
      this.label5.TabIndex = 5;
      this.label5.Text = "Account Name and Description";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label8.ForeColor = System.Drawing.Color.Blue;
      this.label8.Location = new System.Drawing.Point(399, 14);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(192, 24);
      this.label8.TabIndex = 13;
      this.label8.Text = "JOURNAL UPDATE";
      this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label9.Location = new System.Drawing.Point(334, 57);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(34, 16);
      this.label9.TabIndex = 14;
      this.label9.Text = "Date";
      // 
      // dateTimePicker1
      // 
      this.dateTimePicker1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.dateTimePicker1.Location = new System.Drawing.Point(384, 53);
      this.dateTimePicker1.Name = "dateTimePicker1";
      this.dateTimePicker1.Size = new System.Drawing.Size(222, 23);
      this.dateTimePicker1.TabIndex = 1;
      this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
      // 
      // lblDateTime
      // 
      this.lblDateTime.AutoSize = true;
      this.lblDateTime.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblDateTime.ForeColor = System.Drawing.Color.Red;
      this.lblDateTime.Location = new System.Drawing.Point(725, 14);
      this.lblDateTime.Name = "lblDateTime";
      this.lblDateTime.Size = new System.Drawing.Size(49, 16);
      this.lblDateTime.TabIndex = 16;
      this.lblDateTime.Text = "label10";
      // 
      // tbxMonth
      // 
      this.tbxMonth.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxMonth.Location = new System.Drawing.Point(16, 143);
      this.tbxMonth.Name = "tbxMonth";
      this.tbxMonth.Size = new System.Drawing.Size(67, 23);
      this.tbxMonth.TabIndex = 17;
      // 
      // tbxDay
      // 
      this.tbxDay.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxDay.Location = new System.Drawing.Point(89, 143);
      this.tbxDay.Name = "tbxDay";
      this.tbxDay.Size = new System.Drawing.Size(67, 23);
      this.tbxDay.TabIndex = 17;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.ForeColor = System.Drawing.Color.Blue;
      this.label2.Location = new System.Drawing.Point(107, 124);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(29, 16);
      this.label2.TabIndex = 2;
      this.label2.Text = "Day";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.ForeColor = System.Drawing.Color.Blue;
      this.label3.Location = new System.Drawing.Point(29, 95);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(34, 16);
      this.label3.TabIndex = 3;
      this.label3.Text = "Year";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.ForeColor = System.Drawing.Color.Blue;
      this.label4.Location = new System.Drawing.Point(25, 124);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(43, 16);
      this.label4.TabIndex = 4;
      this.label4.Text = "Month";
      // 
      // tbxYear
      // 
      this.tbxYear.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxYear.Location = new System.Drawing.Point(69, 92);
      this.tbxYear.Name = "tbxYear";
      this.tbxYear.Size = new System.Drawing.Size(67, 23);
      this.tbxYear.TabIndex = 17;
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label10.ForeColor = System.Drawing.Color.Blue;
      this.label10.Location = new System.Drawing.Point(617, 124);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(86, 16);
      this.label10.TabIndex = 5;
      this.label10.Text = "Account Code";
      // 
      // tbxDebitAccountCode
      // 
      this.tbxDebitAccountCode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxDebitAccountCode.Location = new System.Drawing.Point(628, 145);
      this.tbxDebitAccountCode.Name = "tbxDebitAccountCode";
      this.tbxDebitAccountCode.Size = new System.Drawing.Size(67, 23);
      this.tbxDebitAccountCode.TabIndex = 17;
      // 
      // tbxCreditAccountCode
      // 
      this.tbxCreditAccountCode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxCreditAccountCode.Location = new System.Drawing.Point(628, 172);
      this.tbxCreditAccountCode.Name = "tbxCreditAccountCode";
      this.tbxCreditAccountCode.Size = new System.Drawing.Size(67, 23);
      this.tbxCreditAccountCode.TabIndex = 17;
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label11.ForeColor = System.Drawing.Color.Blue;
      this.label11.Location = new System.Drawing.Point(746, 124);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(37, 16);
      this.label11.TabIndex = 5;
      this.label11.Text = "Debit";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label12.ForeColor = System.Drawing.Color.Blue;
      this.label12.Location = new System.Drawing.Point(857, 124);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(42, 16);
      this.label12.TabIndex = 5;
      this.label12.Text = "Credit";
      // 
      // tbxDebitAmount
      // 
      this.tbxDebitAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxDebitAmount.Location = new System.Drawing.Point(712, 146);
      this.tbxDebitAmount.Name = "tbxDebitAmount";
      this.tbxDebitAmount.Size = new System.Drawing.Size(110, 22);
      this.tbxDebitAmount.TabIndex = 1;
      this.tbxDebitAmount.TextChanged += new System.EventHandler(this.tbxDebitAmount_TextChanged);
      // 
      // tbxCreditAmount
      // 
      this.tbxCreditAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxCreditAmount.Location = new System.Drawing.Point(828, 172);
      this.tbxCreditAmount.Name = "tbxCreditAmount";
      this.tbxCreditAmount.Size = new System.Drawing.Size(110, 22);
      this.tbxCreditAmount.TabIndex = 1;
      this.tbxCreditAmount.TextChanged += new System.EventHandler(this.tbxCreditAmount_TextChanged);
      // 
      // btnSave
      // 
      this.btnSave.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnSave.Location = new System.Drawing.Point(602, 397);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(132, 28);
      this.btnSave.TabIndex = 20;
      this.btnSave.Text = "&Save this Entry";
      this.btnSave.UseVisualStyleBackColor = true;
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      // 
      // cbxDebitAccountName
      // 
      this.cbxDebitAccountName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.cbxDebitAccountName.FormattingEnabled = true;
      this.cbxDebitAccountName.Location = new System.Drawing.Point(167, 145);
      this.cbxDebitAccountName.Name = "cbxDebitAccountName";
      this.cbxDebitAccountName.Size = new System.Drawing.Size(439, 24);
      this.cbxDebitAccountName.TabIndex = 21;
      this.cbxDebitAccountName.SelectedIndexChanged += new System.EventHandler(this.cbxDebitAccountName_SelectedIndexChanged);
      // 
      // cbxCreditAccountName
      // 
      this.cbxCreditAccountName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.cbxCreditAccountName.FormattingEnabled = true;
      this.cbxCreditAccountName.Location = new System.Drawing.Point(197, 172);
      this.cbxCreditAccountName.Name = "cbxCreditAccountName";
      this.cbxCreditAccountName.Size = new System.Drawing.Size(409, 24);
      this.cbxCreditAccountName.TabIndex = 21;
      this.cbxCreditAccountName.SelectedIndexChanged += new System.EventHandler(this.cbxCreditAccountName_SelectedIndexChanged);
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label13.ForeColor = System.Drawing.Color.Blue;
      this.label13.Location = new System.Drawing.Point(428, 271);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(92, 24);
      this.label13.TabIndex = 13;
      this.label13.Text = "LEDGER";
      this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.ForeColor = System.Drawing.Color.Blue;
      this.label1.Location = new System.Drawing.Point(13, 314);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(37, 16);
      this.label1.TabIndex = 3;
      this.label1.Text = "Debit";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label6.ForeColor = System.Drawing.Color.Blue;
      this.label6.Location = new System.Drawing.Point(402, 314);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(42, 16);
      this.label6.TabIndex = 3;
      this.label6.Text = "Credit";
      // 
      // lblDebitAccountName
      // 
      this.lblDebitAccountName.AutoSize = true;
      this.lblDebitAccountName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblDebitAccountName.ForeColor = System.Drawing.Color.Red;
      this.lblDebitAccountName.Location = new System.Drawing.Point(86, 295);
      this.lblDebitAccountName.Name = "lblDebitAccountName";
      this.lblDebitAccountName.Size = new System.Drawing.Size(101, 16);
      this.lblDebitAccountName.TabIndex = 5;
      this.lblDebitAccountName.Text = "Account Name";
      // 
      // lblDebitAccountCode
      // 
      this.lblDebitAccountCode.AutoSize = true;
      this.lblDebitAccountCode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblDebitAccountCode.ForeColor = System.Drawing.Color.Blue;
      this.lblDebitAccountCode.Location = new System.Drawing.Point(213, 314);
      this.lblDebitAccountCode.Name = "lblDebitAccountCode";
      this.lblDebitAccountCode.Size = new System.Drawing.Size(40, 16);
      this.lblDebitAccountCode.TabIndex = 3;
      this.lblDebitAccountCode.Text = "Code";
      this.lblDebitAccountCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblCreditAccountCode
      // 
      this.lblCreditAccountCode.AutoSize = true;
      this.lblCreditAccountCode.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblCreditAccountCode.ForeColor = System.Drawing.Color.Blue;
      this.lblCreditAccountCode.Location = new System.Drawing.Point(709, 314);
      this.lblCreditAccountCode.Name = "lblCreditAccountCode";
      this.lblCreditAccountCode.Size = new System.Drawing.Size(40, 16);
      this.lblCreditAccountCode.TabIndex = 3;
      this.lblCreditAccountCode.Text = "Code";
      this.lblCreditAccountCode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label14.ForeColor = System.Drawing.Color.Blue;
      this.label14.Location = new System.Drawing.Point(507, 314);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(37, 16);
      this.label14.TabIndex = 3;
      this.label14.Text = "Debit";
      // 
      // label15
      // 
      this.label15.AutoSize = true;
      this.label15.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label15.ForeColor = System.Drawing.Color.Blue;
      this.label15.Location = new System.Drawing.Point(896, 314);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(42, 16);
      this.label15.TabIndex = 3;
      this.label15.Text = "Credit";
      // 
      // lblCreditAccountName
      // 
      this.lblCreditAccountName.AutoSize = true;
      this.lblCreditAccountName.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblCreditAccountName.ForeColor = System.Drawing.Color.Red;
      this.lblCreditAccountName.Location = new System.Drawing.Point(566, 295);
      this.lblCreditAccountName.Name = "lblCreditAccountName";
      this.lblCreditAccountName.Size = new System.Drawing.Size(101, 16);
      this.lblCreditAccountName.TabIndex = 5;
      this.lblCreditAccountName.Text = "Account Name";
      // 
      // btnShowAll
      // 
      this.btnShowAll.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnShowAll.Location = new System.Drawing.Point(218, 397);
      this.btnShowAll.Name = "btnShowAll";
      this.btnShowAll.Size = new System.Drawing.Size(75, 28);
      this.btnShowAll.TabIndex = 20;
      this.btnShowAll.Text = "Show &All";
      this.btnShowAll.UseVisualStyleBackColor = true;
      this.btnShowAll.Click += new System.EventHandler(this.btnShowAll_Click);
      // 
      // textBox3
      // 
      this.textBox3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox3.Location = new System.Drawing.Point(14, 338);
      this.textBox3.Name = "textBox3";
      this.textBox3.Size = new System.Drawing.Size(207, 23);
      this.textBox3.TabIndex = 24;
      // 
      // textBox4
      // 
      this.textBox4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox4.Location = new System.Drawing.Point(237, 338);
      this.textBox4.Name = "textBox4";
      this.textBox4.Size = new System.Drawing.Size(207, 23);
      this.textBox4.TabIndex = 24;
      // 
      // textBox5
      // 
      this.textBox5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox5.Location = new System.Drawing.Point(510, 338);
      this.textBox5.Name = "textBox5";
      this.textBox5.Size = new System.Drawing.Size(207, 23);
      this.textBox5.TabIndex = 24;
      // 
      // textBox6
      // 
      this.textBox6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBox6.Location = new System.Drawing.Point(733, 338);
      this.textBox6.Name = "textBox6";
      this.textBox6.Size = new System.Drawing.Size(207, 23);
      this.textBox6.TabIndex = 24;
      // 
      // btnNew
      // 
      this.btnNew.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnNew.Location = new System.Drawing.Point(355, 397);
      this.btnNew.Name = "btnNew";
      this.btnNew.Size = new System.Drawing.Size(75, 28);
      this.btnNew.TabIndex = 20;
      this.btnNew.Text = "&New";
      this.btnNew.UseVisualStyleBackColor = true;
      this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
      // 
      // btnReset
      // 
      this.btnReset.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnReset.Location = new System.Drawing.Point(483, 397);
      this.btnReset.Name = "btnReset";
      this.btnReset.Size = new System.Drawing.Size(75, 28);
      this.btnReset.TabIndex = 20;
      this.btnReset.Text = "&Reset";
      this.btnReset.UseVisualStyleBackColor = true;
      this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
      // 
      // tbxDescription
      // 
      this.tbxDescription.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxDescription.Location = new System.Drawing.Point(167, 200);
      this.tbxDescription.Name = "tbxDescription";
      this.tbxDescription.Size = new System.Drawing.Size(439, 23);
      this.tbxDescription.TabIndex = 24;
      // 
      // chkSoDu
      // 
      this.chkSoDu.AutoSize = true;
      this.chkSoDu.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.chkSoDu.ForeColor = System.Drawing.SystemColors.ActiveCaption;
      this.chkSoDu.Location = new System.Drawing.Point(32, 176);
      this.chkSoDu.Name = "chkSoDu";
      this.chkSoDu.Size = new System.Drawing.Size(101, 20);
      this.chkSoDu.TabIndex = 25;
      this.chkSoDu.Text = "So du dau ky";
      this.chkSoDu.UseVisualStyleBackColor = true;
      // 
      // frmUpdate
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(952, 442);
      this.Controls.Add(this.chkSoDu);
      this.Controls.Add(this.textBox6);
      this.Controls.Add(this.textBox5);
      this.Controls.Add(this.textBox4);
      this.Controls.Add(this.tbxDescription);
      this.Controls.Add(this.textBox3);
      this.Controls.Add(this.cbxCreditAccountName);
      this.Controls.Add(this.cbxDebitAccountName);
      this.Controls.Add(this.btnShowAll);
      this.Controls.Add(this.btnReset);
      this.Controls.Add(this.btnNew);
      this.Controls.Add(this.btnSave);
      this.Controls.Add(this.tbxCreditAccountCode);
      this.Controls.Add(this.tbxDebitAccountCode);
      this.Controls.Add(this.tbxDay);
      this.Controls.Add(this.tbxMonth);
      this.Controls.Add(this.tbxYear);
      this.Controls.Add(this.lblDateTime);
      this.Controls.Add(this.dateTimePicker1);
      this.Controls.Add(this.label9);
      this.Controls.Add(this.label13);
      this.Controls.Add(this.label8);
      this.Controls.Add(this.label12);
      this.Controls.Add(this.label11);
      this.Controls.Add(this.label10);
      this.Controls.Add(this.lblCreditAccountName);
      this.Controls.Add(this.lblDebitAccountName);
      this.Controls.Add(this.label5);
      this.Controls.Add(this.label15);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.label14);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.lblCreditAccountCode);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.lblDebitAccountCode);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.tbxCreditAmount);
      this.Controls.Add(this.tbxDebitAmount);
      this.MaximizeBox = false;
      this.Name = "frmUpdate";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = "Transaction Record";
      this.Load += new System.EventHandler(this.frmJournal_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.DateTimePicker dateTimePicker1;
    private System.Windows.Forms.Label lblDateTime;
    private System.Windows.Forms.TextBox tbxMonth;
    private System.Windows.Forms.TextBox tbxDay;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox tbxYear;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.TextBox tbxDebitAccountCode;
    private System.Windows.Forms.TextBox tbxCreditAccountCode;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.TextBox tbxDebitAmount;
    private System.Windows.Forms.TextBox tbxCreditAmount;
    private System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.ComboBox cbxDebitAccountName;
    private System.Windows.Forms.ComboBox cbxCreditAccountName;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label lblDebitAccountName;
    private System.Windows.Forms.Label lblDebitAccountCode;
    private System.Windows.Forms.Label lblCreditAccountCode;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.Label lblCreditAccountName;
    private System.Windows.Forms.Button btnShowAll;
    private System.Windows.Forms.TextBox textBox3;
    private System.Windows.Forms.TextBox textBox4;
    private System.Windows.Forms.TextBox textBox5;
    private System.Windows.Forms.TextBox textBox6;
    private System.Windows.Forms.Button btnNew;
    private System.Windows.Forms.Button btnReset;
    private System.Windows.Forms.TextBox tbxDescription;
    private System.Windows.Forms.CheckBox chkSoDu;
  }
}

